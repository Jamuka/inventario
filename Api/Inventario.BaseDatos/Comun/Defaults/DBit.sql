﻿CREATE DEFAULT [Comun].[DBit]
    AS 1;


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DBit]', @objname = N'[Inventario].[EquipoRegistro].[Activo]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DBit]', @objname = N'[Seguridad].[Permiso].[Activo]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DBit]', @objname = N'[Seguridad].[Rol].[Activo]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DBit]', @objname = N'[Inventario].[Caracteristica].[Activo]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DBit]', @objname = N'[Inventario].[Estado].[Activo]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DBit]', @objname = N'[Inventario].[Tipo].[Activo]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DBit]', @objname = N'[Ubicacion].[Empresa].[Activo]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DBit]', @objname = N'[Ubicacion].[Sucursal].[Activo]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DBit]', @objname = N'[Seguridad].[Usuario].[Activo]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DBit]', @objname = N'[Inventario].[Equipo].[Activo]';

