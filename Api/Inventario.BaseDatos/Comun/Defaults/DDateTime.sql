﻿CREATE DEFAULT [Comun].[DDateTime]
    AS GetDate();


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Inventario].[Equipo].[FechaInsercion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Inventario].[Equipo].[FechaActualizacion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Seguridad].[Usuario].[FechaInsercion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Seguridad].[Usuario].[FechaActualizacion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Ubicacion].[Sucursal].[FechaInsercion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Ubicacion].[Sucursal].[FechaActualizacion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Inventario].[EquipoCaracteristica].[FechaInsercion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Inventario].[EquipoCaracteristica].[FechaActualizacion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Seguridad].[Entradas].[FechaEntrada]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Seguridad].[Entradas].[FechaInsercion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Ubicacion].[Empresa].[FechaInsercion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Ubicacion].[Empresa].[FechaActualizacion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Inventario].[Tipo].[FechaInsercion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Inventario].[Tipo].[FechaActualizacion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Inventario].[TipoCaracteristica].[FechaInsercion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Inventario].[TipoCaracteristica].[FechaActualizacion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Inventario].[Historia].[FechaInsercion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Inventario].[Caracteristica].[FechaInsercion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Inventario].[Caracteristica].[FechaActualizacion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Inventario].[Estado].[FechaInsercion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Inventario].[Estado].[FechaActualizacion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Seguridad].[Rol].[FechaInsercion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Seguridad].[Rol].[FechaActualizacion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Seguridad].[RolPermiso].[FechaInsercion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Seguridad].[UsuarioRol].[FechaInsercion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Seguridad].[Permiso].[FechaInsercion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Seguridad].[Permiso].[FechaActualizacion]';


GO
EXECUTE sp_bindefault @defname = N'[Comun].[DDateTime]', @objname = N'[Inventario].[EquipoRegistro].[FechaInsercion]';

