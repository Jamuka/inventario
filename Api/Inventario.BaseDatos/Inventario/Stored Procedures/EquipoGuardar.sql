﻿CREATE PROCEDURE [Inventario].[EquipoGuardar]
	@IdEquipo int, 
	@IdTipo int, 
	@IdEstado int, 
	@IdSucursal int, 
	@IdUsuario int, 
	@Nombre nvarchar(50),	
	@ActivoFijo nvarchar(20)=null,	
	@Marca nvarchar(50) = null,
	@Modelo nvarchar(20) = null,
	@Serie nvarchar(30) = null,
	@Numero smallint,
	@Fechacompra datetime,
	@idUsuarioCaptura int,
	@Activo bit = 1,
	@Caracteristicas  [Inventario].[TEquipoCaracteristica] readonly
as
 begin
	if(@IdEquipo > 0)
	 begin
		declare @vIdTipo int, @vIdEstado int, @vIdSucursal int,	@vIdUsuario int, @vNombre nvarchar(50),	@vActivoFijo nvarchar(20) = null, 
				@vMarca nvarchar(50) = null, @vModelo nvarchar(20) = null, @vSerie nvarchar(30) = null, @vNumero smallint = null, @vFechacompra datetime, 
				@vActivo bit = 1, @vIdUsuarioCaptura int

		select @vIdTipo = IdTipo, @vIdEstado = IdEstado, @vIdSucursal = IdSucursal,	@vIdUsuario = IdUsuario, @vNombre = Nombre,	@vActivoFijo = ActivoFijo,
			   @vMarca = Marca, @vModelo = Modelo, @vSerie = Serie, @vNumero = Numero, @vFechacompra = FechaCompra, @vActivo = Activo, @vIdUsuarioCaptura = IdusuarioCaptura
		  from [Inventario].[Equipo] where idEquipo = @IdEquipo

		  if(iif(@IdTipo = @vIdTipo, 1,0) = 0 or iif(@IdSucursal = @vIdSucursal, 1, 0) = 0 or iif(@IdUsuario = @vIdUsuario, 1, 0) = 0 or 
			 iif(@Nombre = @vNombre, 1, 0) = 0 or iif(@ActivoFijo = @vActivoFijo, 1, 0) = 0 or iif(@Marca = @vMarca, 1, 0) = 0 or 
			 iif(@Modelo = @vModelo, 1, 0) = 0 or iif(@Serie = @vSerie, 1, 0) = 0 or iif(@Numero = @vNumero, 1, 0) = 0 or 
			 iif(@Fechacompra = @vFechacompra, 1, 0) = 0 or iif(@Activo = @vActivo, 1, 0) = 0)
			begin
				insert into [Inventario].[EquipoRegistro](IdEquipo, IdTipo, IdEstado, IdSucursal, IdUsuario, Nombre, ActivoFijo, Marca, Modelo, Serie, Numero, FechaCompra, IdUsuarioCaptura, Activo)
					 values(@IdEquipo, @vIdTipo, @vIdEstado, @vIdSucursal, @vIdUsuario, @vNombre, @vActivoFijo, @vMarca, @vModelo, @vSerie, @vNumero, @vFechaCompra, @vidUsuarioCaptura, @vActivo)

				update [Inventario].[Equipo]
				   set IdTipo= @IdTipo,
					   IdEstado = @IdEstado,
					   IdSucursal = @IdSucursal,
					   IdUsuario = @IdUsuario,
					   Nombre = @Nombre,
					   ActivoFijo = @ActivoFijo,
					   Marca = @Marca,
					   Modelo = @Modelo,
					   Serie = @Serie,
					   Numero = @Numero,
					   FechaCompra = @Fechacompra,
					   Activo = @Activo,
					   IdUsuarioCaptura = @idUsuarioCaptura,
					   FechaActualizacion = getdate()
				 where IdEquipo = @IdEquipo
			end

	 end
	else
	 begin
		if(not exists(select * from [Inventario].[Equipo] where Nombre = @Nombre))
		 begin
			insert into [Inventario].[Equipo](IdTipo, IdEstado, IdSucursal, IdUsuario, Nombre, ActivoFijo, Marca, Modelo, Serie, Numero, FechaCompra, IdUsuarioCaptura)
			values (@IdTipo, @IdEstado, @IdSucursal, @IdUsuario, @Nombre, @ActivoFijo, @Marca, @Modelo, @Serie, @Numero, @Fechacompra, @idUsuarioCaptura)

			set @IdEquipo = SCOPE_IDENTITY()
		 end
	 end

	  MERGE [Inventario].[EquipoCaracteristica] AS target
        USING @Caracteristicas AS source
        ON (target.idEquipo = @IdEquipo AND target.idCaracteristica = source.idCaracteristica)
        WHEN NOT MATCHED BY TARGET THEN
            INSERT (idEquipo, idCaracteristica, Valor)
            VALUES (@IdEquipo, source.idCaracteristica, source.Valor)
        WHEN MATCHED THEN 
            UPDATE SET target.Valor = source.Valor;

 end