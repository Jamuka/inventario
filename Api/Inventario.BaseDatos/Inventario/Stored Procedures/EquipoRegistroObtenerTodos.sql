﻿--exec [Inventario].[EquipoRegistroObtenerTodos] 1
CREATE PROCEDURE [Inventario].[EquipoRegistroObtenerTodos]
	@idEquipo int
as
 begin
	select * from(	
	select idEquipo, idTipo, idEstado, idSucursal, IdUsuario, Nombre, ActivoFijo, Marca, 
		   Modelo, Serie, Numero, FechaCompra, Activo, FechaActualizacion, FechaActualizacion as FechaInsercion, IdUsuarioCaptura
	  from [Inventario].[Equipo]
	 where idEquipo = @idEquipo
	union
	select idEquipo, idTipo, idEstado, idSucursal, IdUsuario, Nombre, ActivoFijo, Marca, 
		   Modelo, Serie, Numero, FechaCompra, Activo, FechaInsercion as FechaActualizacion, FechaInsercion, IdUsuarioCaptura
	  from [Inventario].[EquipoRegistro]
	 where idEquipo = @idEquipo
 ) x order by FechaActualizacion desc
 end