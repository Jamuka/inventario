﻿CREATE PROCEDURE [Inventario].[EstadoGuardar]
	@IdEstado int, 
	@Nombre nvarchar(50),	
	@Activo bit = 1
as
 begin
	if(@IdEstado > 0)
	 begin
		update [Inventario].[Estado]
		   set Nombre=@Nombre,
			   Activo = @Activo
		 where IdEstado = @IdEstado
	 end
	else
	 begin
		if(not exists(select * from [Inventario].[Estado] where Nombre = @Nombre))
		 begin
			insert into [Inventario].[Estado](Nombre)
			values (@Nombre)
		 end
	 end

 end