﻿Create PROCEDURE [Inventario].[HistoriaGuardar]
	@IdHistoria int, 
	@IdUsuario int, 
	@IdEquipo int, 
	@IdSucursal int, 	
	@Comentarios nvarchar(50)
as
 begin
	if(@IdHistoria > 0)
	 begin
		update [Inventario].[Historia]
		   set IdUsuario = @IdUsuario,
			   IdEquipo = @IdEquipo,
			   IdSucursal = @IdSucursal,
			   Comentarios = @Comentarios
		 where IdHistoria = @IdHistoria
	 end
	else
	 begin
		insert into [Inventario].[Historia](IdUsuario, IdEquipo, IdSucursal, Comentarios)
		values (@IdUsuario, @IdEquipo, @IdSucursal, @Comentarios)
	 end
 end