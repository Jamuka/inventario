﻿CREATE PROCEDURE [Inventario].[TipoCaracteristicaObtenerTodos]
	@idTipo int
as
 begin
        select * from [Inventario].[Tipo] where idTipo = @idTipo

		select c.*, 
			   case 
					when tc.idTipo is null then cast(0 as bit)
					else cast(1 as bit)
				end as Asignado
		  from [Inventario].[Caracteristica] c
	 left join [Inventario].[TipoCaracteristica] tc on tc.IdCaracteristica = c.IdCaracteristica and tc.IdTipo =@idTipo
		 where c.Activo = 1

end