﻿CREATE PROCEDURE [Inventario].[TipoGuardar]
	@IdTipo int, 
	@Nombre nvarchar(50),	
	@Activo bit = 1
as
 begin
	if(@IdTipo > 0)
	 begin
		update [Inventario].[Tipo]
		   set Nombre=@Nombre,
			   Activo = @Activo
		 where IdTipo = @IdTipo
	 end
	else
	 begin
		if(not exists(select * from [Inventario].[Tipo] where Nombre = @Nombre))
		 begin
			insert into [Inventario].[Tipo](Nombre)
			values (@Nombre)
		 end
	 end

 end