﻿CREATE TABLE [Inventario].[Caracteristica] (
    [IdCaracteristica]   INT           IDENTITY (1, 1) NOT NULL,
    [Nombre]             NVARCHAR (50) NOT NULL,
    [Activo]             BIT           NOT NULL,
    [FechaInsercion]     DATETIME      NOT NULL,
    [FechaActualizacion] DATETIME      NOT NULL,
    CONSTRAINT [PK_Caracteristica] PRIMARY KEY CLUSTERED ([IdCaracteristica] ASC)
);

