﻿CREATE TABLE [Inventario].[EquipoCaracteristica] (
    [idEquipoCaracteristica] INT            IDENTITY (1, 1) NOT NULL,
    [IdEquipo]               INT            NOT NULL,
    [IdCaracteristica]       INT            NOT NULL,
    [Valor]                  NVARCHAR (500) NULL,
    [FechaInsercion]         DATETIME       NOT NULL,
    [FechaActualizacion]     DATETIME       NOT NULL,
    CONSTRAINT [FK_EquipoCaracteristica_Caracteristica] FOREIGN KEY ([IdCaracteristica]) REFERENCES [Inventario].[Caracteristica] ([IdCaracteristica]),
    CONSTRAINT [FK_EquipoCaracteristica_Equipo] FOREIGN KEY ([IdEquipo]) REFERENCES [Inventario].[Equipo] ([IdEquipo])
);

