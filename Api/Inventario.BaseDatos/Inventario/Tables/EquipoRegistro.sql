﻿CREATE TABLE [Inventario].[EquipoRegistro] (
    [idEquipoRegistro] INT           IDENTITY (1, 1) NOT NULL,
    [IdEquipo]         INT           NOT NULL,
    [IdTipo]           INT           NOT NULL,
    [IdEstado]         INT           NOT NULL,
    [IdSucursal]       INT           NULL,
    [IdUsuario]        INT           NULL,
    [Nombre]           NVARCHAR (50) NOT NULL,
    [ActivoFijo]       NVARCHAR (20) NULL,
    [Marca]            NVARCHAR (50) NULL,
    [Modelo]           NVARCHAR (20) NULL,
    [Serie]            NVARCHAR (30) NULL,
    [Numero]           SMALLINT      NULL,
    [FechaCompra]      DATETIME      NULL,
    [IdUsuarioCaptura] INT           NULL,
    [Activo]           BIT           NOT NULL,
    [FechaInsercion]   DATETIME      NOT NULL,
    CONSTRAINT [PK_EquipoRegistro] PRIMARY KEY CLUSTERED ([idEquipoRegistro] ASC),
    CONSTRAINT [FK_EquipoRegistro_Equipo] FOREIGN KEY ([IdEquipo]) REFERENCES [Inventario].[Equipo] ([IdEquipo]),
    CONSTRAINT [FK_EquipoRegistro_Usuario] FOREIGN KEY ([IdUsuarioCaptura]) REFERENCES [Seguridad].[Usuario] ([IdUsuario]),
    CONSTRAINT [FK_EquipoRegistro_UsuarioCaptura] FOREIGN KEY ([IdUsuarioCaptura]) REFERENCES [Seguridad].[Usuario] ([IdUsuario])
);



