﻿CREATE TABLE [Inventario].[Historia] (
    [IdHistoria]     INT            IDENTITY (1, 1) NOT NULL,
    [IdUsuario]      INT            NOT NULL,
    [IdEquipo]       INT            NOT NULL,
    [IdSucursal]     INT            NOT NULL,
    [Comentarios]    NVARCHAR (300) NULL,
    [FechaInsercion] DATETIME       NOT NULL,
    CONSTRAINT [PK_Historia] PRIMARY KEY CLUSTERED ([IdHistoria] ASC),
    CONSTRAINT [FK_Historia_Equipo] FOREIGN KEY ([IdEquipo]) REFERENCES [Inventario].[Equipo] ([IdEquipo]),
    CONSTRAINT [FK_Historia_Sucursal] FOREIGN KEY ([IdSucursal]) REFERENCES [Ubicacion].[Sucursal] ([IdSucursal]),
    CONSTRAINT [FK_Historia_Usuario] FOREIGN KEY ([IdUsuario]) REFERENCES [Seguridad].[Usuario] ([IdUsuario])
);

