﻿CREATE TYPE [Inventario].[TEquipoCaracteristica] AS TABLE (
    [idCaracteristica] INT            NULL,
    [valor]            NVARCHAR (500) NULL);

