﻿CREATE PROCEDURE [Seguridad].[RolPermisoObtenerTodos]
	@idRol int
as
 begin
        select * from [Seguridad].[Rol] where idRol = @idRol

		select p.*, 
			   case 
					when rp.idRol is null then cast(0 as bit)
					else cast(1 as bit)
				end as Concedido
		  from [Seguridad].[Permiso] p
	 left join [Seguridad].[RolPermiso] rp on rp.idPermiso = p.IdPermiso and rp.idRol =@idRol
		 where p.Activo = 1

end