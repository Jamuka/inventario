﻿--exec [Seguridad].[UsuarioObtener] @Identificacion='admin'
CREATE PROCEDURE [Seguridad].[UsuarioObtener]
    @Identificacion nvarchar(50)
as
 begin
	   select u.* 
		 from [Seguridad].[Usuario] u
   inner join [Seguridad].[UsuarioRol] ur on ur.IdUsuario = u.IdUsuario
   inner join [Seguridad].[Rol] r on r.IdRol = ur.IdRol
		WHERE u.Identificacion = @Identificacion and u.Activo = 1
		  and r.Nombre <> 'Usuario'

        select distinct p.IdPermiso, p.IdPermisoPadre, p.Nombre, p.Texto, p.Icono, p.Orden, 
               p.Activo, p.FechaInsercion, p.FechaActualizacion
          from [Seguridad].[Rol] r
    inner join [Seguridad].[RolPermiso] rp on rp.idRol = r.idRol
    inner join [Seguridad].[Permiso] p on p.idPermiso = rp.idPermiso
         where p.Activo = 1 and r.idRol in 
                        (
                            select IdRol from [Seguridad].[Usuario] u
                        inner join [Seguridad].[UsuarioRol] ur on ur.idUsuario = u.idUsuario
                             WHERE Identificacion = @Identificacion
                        )
      order by p.Orden

	   select r.* 
		 from [Seguridad].[Usuario] u
   inner join [Seguridad].[UsuarioRol] ur on ur.IdUsuario = u.IdUsuario
   inner join [Seguridad].[Rol] r on r.IdRol = ur.IdRol
		WHERE u.Identificacion = @Identificacion and u.Activo = 1
 end