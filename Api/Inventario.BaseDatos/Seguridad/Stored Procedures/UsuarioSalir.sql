﻿Create PROCEDURE [Seguridad].[UsuarioSalir]
    @idUsuario int,
    @idSesion nvarchar(36)
as
 begin
	update [Seguridad].Entradas set FechaSalida = getdate()
        where idUsuario = @idUsuario and idSesion = @idSesion
 end