﻿CREATE TABLE [Seguridad].[UsuarioRol] (
    [IdUsuarioRol]   INT      IDENTITY (1, 1) NOT NULL,
    [IdUsuario]      INT      NOT NULL,
    [IdRol]          INT      NOT NULL,
    [FechaInsercion] DATETIME NOT NULL,
    CONSTRAINT [PK_UsuarioRol] PRIMARY KEY CLUSTERED ([IdUsuarioRol] ASC),
    CONSTRAINT [FK_UsuarioRol_Rol] FOREIGN KEY ([IdRol]) REFERENCES [Seguridad].[Rol] ([IdRol]),
    CONSTRAINT [FK_UsuarioRol_Usuario] FOREIGN KEY ([IdUsuario]) REFERENCES [Seguridad].[Usuario] ([IdUsuario])
);

