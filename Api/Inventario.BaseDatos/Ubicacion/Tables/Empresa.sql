﻿CREATE TABLE [Ubicacion].[Empresa] (
    [IdEmpresa]          INT            IDENTITY (1, 1) NOT NULL,
    [Nombre]             NVARCHAR (50)  NOT NULL,
    [UrlLogo]            NVARCHAR (200) NULL,
    [Activo]             BIT            NOT NULL,
    [FechaInsercion]     DATETIME       NOT NULL,
    [FechaActualizacion] DATETIME       NOT NULL,
    CONSTRAINT [PK_Empresa] PRIMARY KEY CLUSTERED ([IdEmpresa] ASC)
);



