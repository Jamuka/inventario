﻿CREATE TABLE [Ubicacion].[Sucursal] (
    [IdSucursal]         INT           IDENTITY (1, 1) NOT NULL,
    [IdEmpresa]          INT           NOT NULL,
    [Nombre]             NVARCHAR (50) NOT NULL,
    [Activo]             BIT           NOT NULL,
    [FechaInsercion]     DATETIME      NOT NULL,
    [FechaActualizacion] DATETIME      NOT NULL,
    CONSTRAINT [PK_Sucursal] PRIMARY KEY CLUSTERED ([IdSucursal] ASC),
    CONSTRAINT [FK_Sucursal_Empresa] FOREIGN KEY ([IdEmpresa]) REFERENCES [Ubicacion].[Empresa] ([IdEmpresa])
);

