﻿using System;
using System.Collections.Generic;

namespace Inventario.Comun.Inventario
{
    public class ModeloEquipo : ModeloCatalogo
    {
        public int IdEquipo
        {
            get;
            set;
        }

        public int IdTipo
        {
            get;
            set;
        }

        public int IdEstado
        {
            get;
            set;
        }

        public int IdSucursal
        {
            get;
            set;
        }

        public int? IdUsuario
        {
            get;
            set;
        }

        public string ActivoFijo
        {
            get;
            set;
        }

        public string Marca
        {
            get;
            set;
        }

        public string Modelo
        {
            get;
            set;
        }

        public string Serie
        {
            get;
            set;
        }

        public short? Numero
        {
            get;
            set;
        }

        public DateTime FechaCompra
        {
            get;
            set;
        }

        public List<ModeloCaracteristica> Caracteristicas
        {
            get;
            set;
        }

        public int IdUsuarioCaptura
        {
            get;
            set;
        }
    }
}
