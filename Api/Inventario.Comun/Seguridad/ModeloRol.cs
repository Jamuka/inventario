﻿using System.Collections.Generic;

namespace Inventario.Comun.Seguridad
{
    public class ModeloRol : ModeloCatalogo
    {
        public int IdRol
        {
            get;
            set;
        }

        public bool? Asignado
        {
            get;
            set;
        }

        public List<ModeloPermiso> Permisos
        {
            get;
            set;
        }
    }
}
