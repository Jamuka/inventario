﻿namespace Inventario.Comun.Seguridad
{
    public class ModeloSolicitudEntrar
    {
        public string Identificacion
        {
            get;
            set;
        }

        public string Contrasena
        {
            get;
            set;
        }

        public string DireccionIp
        {
            get;
            set;
        }

        public int? IdSucursal
        {
            get;
            set;
        }
    }
}
