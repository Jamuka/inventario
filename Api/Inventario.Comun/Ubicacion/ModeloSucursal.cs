﻿namespace Inventario.Comun.Ubicacion
{
    public class ModeloSucursal : ModeloCatalogo
    {
        public int IdSucursal
        {
            get;
            set;
        }

        public int IdEmpresa
        {
            get;
            set;
        }
    }
}
