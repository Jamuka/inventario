﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Inventario.Libreria.MsSql;

namespace Inventario.Libreria
{
    public class AccesoDatos : IDisposable
    {
       
        private readonly ConnectionString dbConnectionString;
        private readonly ConcurrentDictionary<string, Command> comandos;

        public AccesoDatos()
        {

        }

        public AccesoDatos(string nombreCadenaConexion, string cadenaConexion, IEnumerable<string> procedimientosAlmacenados)
            : this(new ConnectionString(nombreCadenaConexion, cadenaConexion))
        {
            foreach (var procedimientoAlmacenado in procedimientosAlmacenados)
            {
                this.AgregarComando(procedimientoAlmacenado);
            }
        }

        public AccesoDatos(ConnectionString cadenaConexion)
           : this(cadenaConexion, cadenaConexion.DefaultCommandTimeout ?? Command.CommandTimeout)
        { }

        public AccesoDatos(ConnectionString cadenaConexion, int tiempoEspera)
        {
            this.dbConnectionString = new ConnectionString(cadenaConexion);
            this.comandos = new ConcurrentDictionary<string, Command>();
            this.ComandoTiempoEspera = tiempoEspera;
        }

        public IEnumerable<Command> Comandos
        {
            get
            {
                return comandos.Values;
            }
        }

        public int ComandoTiempoEspera
        {
            get;
            private set;
        }

        public int Reintentos
        {
            get;
            set;
        }

        public int TiempoReintento
        {
            get;
            set;
        }

        public Command AgregarComando(string nombre, int tiempoEspera)
        {
            Command comando;
            if (comandos.ContainsKey(nombre))
            {
                comando = ObtenerComando(nombre);
            }
            else
            {
                comando = new Command(nombre, dbConnectionString.connectionString)
                {
                    Timeout = Command.CommandTimeout,
                    RetryCount = Command.CommandRetryCount,
                    RetryDelay = Command.CommandRetryDelay
                };
                comandos.TryAdd(nombre, comando);
            }
            return comando;
        }

        public Command AgregarComando(string nombre)
        {
            return AgregarComando(nombre, this.ComandoTiempoEspera);
        }

        public void EliminarComando(string nombre)
        {
            Command comando;
            comandos.TryRemove(nombre, out comando);
        }

        public Command ObtenerComando(string nombre)
        {
            Command dbCommand;
            if (comandos.TryGetValue(nombre, out dbCommand))
            {
                return (Command)dbCommand.Clone();
            }
            else
            {
                return (Command)AgregarComando(nombre);
            }
            throw new KeyNotFoundException(string.Format("{0} {1} {2}", "Command", nombre, "doesn't exist as added command"));
        }

        public void LimpiarComandos()
        {
            comandos.Clear();
        }

        protected TParameter ObtenerValorParametro<TParameter>(IDictionary<string, object> parametros, string nombreParametro)
        {
            try
            {
                try
                {
                    return (TParameter)Convert.ChangeType(parametros[nombreParametro], typeof(TParameter));
                }
                catch
                {
                    return (TParameter)parametros[nombreParametro];
                }
            }
            catch (Exception ex)
            {
                throw new DataException(String.Format("Error getting parameter '{0}' value: {1}", nombreParametro, ex.Message));
            }
        }

        protected TField ObtenerValorCampo<TField>(DataRow registro, string nombreCampo)
        {
            try
            {
                try
                {
                    return (TField)Convert.ChangeType(registro[nombreCampo], typeof(TField));
                }
                catch
                {
                    return (TField)registro[nombreCampo];
                }
            }
            catch (Exception ex)
            {
                throw new DataException(String.Format("Error getting field '{0}' value: {1}", nombreCampo, ex.Message));
            }
        }

        public void Dispose()
        {
            foreach (var dbCommand in comandos)
            {
                dbCommand.Value.Dispose();
            }
            SqlConnection.ClearAllPools();
            GC.SuppressFinalize(this);
        }

        public override string ToString()
        {
            return string.Format("Commands: {0}", comandos.Count);
        }
    }
}
