﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Inventario.Libreria
{
    public static class Extension
    {
        public static ReadOnlyDictionary<TKey, TValue> ADiccionarioSoloLectura<TKey, TValue>(this Dictionary<TKey, TValue> dictionary)
        {
            return new ReadOnlyDictionary<TKey, TValue>(dictionary);
        }

        public static T Clonar<T>(T source)
        {
            using (var memoryStream = new System.IO.MemoryStream())
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(memoryStream, source);
                memoryStream.Position = 0;
                return (T)binaryFormatter.Deserialize(memoryStream);
            }
        }
    }    
}
