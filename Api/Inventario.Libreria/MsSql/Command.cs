﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Inventario.Libreria.Enum;

namespace Inventario.Libreria.MsSql
{
    [Serializable]
    public class Command : IDisposable
    {
        internal const int CommandTimeout = 30000 / 1000;
        internal const int CommandRetryCount = 3;
        internal const int CommandRetryDelay = 1000;
        private readonly string connectionString;
        private SqlCommand command;
        private SqlCommand commandAsync;
        private ParameterList parameterList;

        public Command()
        {
            Timeout = CommandTimeout;
            RetryCount = CommandRetryCount;
            RetryDelay = CommandRetryDelay;
        }

        public Command(string name, string connectionString)
            : this()
        {
            this.connectionString = connectionString;
            Name = name;
            SetParameters();
        }

        public string Name
        {
            get;
            private set;
        }

        public int Timeout
        {
            get;
            set;
        }

        public int RetryCount
        {
            get;
            set;
        }

        public int RetryDelay
        {
            get;
            set;
        }

        public IDataReader ExecuteReader(ReadOnlyDictionary<string, object> inputParameters, out ReadOnlyDictionary<string, object> outputParameters)
        {
            var output = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var result = this.Execute<IDataReader>(ExecuteType.Reader, inputParameters, output);
            outputParameters = new ReadOnlyDictionary<string, object>(output);
            return result;
        }

        public IDataReader ExecuteReader(ReadOnlyDictionary<string, object> inputParameters)
        {
            return this.Execute<IDataReader>(ExecuteType.Reader, inputParameters);
        }

        public IDataReader ExecuteReader()
        {
            return this.Execute<IDataReader>(ExecuteType.Reader);
        }

        public IAsyncResult ExecuteReaderAsync(ReadOnlyDictionary<string, object> inputParameters, out ReadOnlyDictionary<string, object> outputParameters)
        {
            var output = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var result = this.Execute<IAsyncResult>(ExecuteType.ReaderAsync, inputParameters, output);
            outputParameters = new ReadOnlyDictionary<string, object>(output);
            return result;
        }

        public IAsyncResult ExecuteReaderAsync(ReadOnlyDictionary<string, object> inputParameters)
        {
            return this.Execute<IAsyncResult>(ExecuteType.ReaderAsync, inputParameters);
        }

        public IAsyncResult ExecuteReaderAsync()
        {
            return this.Execute<IAsyncResult>(ExecuteType.ReaderAsync);
        }

        public DataTable ExecuteTable(ReadOnlyDictionary<string, object> inputParameters, out ReadOnlyDictionary<string, object> outputParameters)
        {
            var output = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var result = this.Execute<DataTable>(ExecuteType.Table, inputParameters, output);
            outputParameters = new ReadOnlyDictionary<string, object>(output);
            return result;
        }

        public DataTable ObtenerTabla(ReadOnlyDictionary<string, object> parametrosEntrada)
        {
            ReadOnlyDictionary<string, object> outputParameters;
            return this.ExecuteTable(parametrosEntrada, out outputParameters);
        }

        public DataTable ObtenerTabla()
        {
            return this.ObtenerTabla(new Dictionary<string, object>().ADiccionarioSoloLectura());
        }

        public int EjecutarSinConsultar(ReadOnlyDictionary<string, object> parametrosEntrada, out ReadOnlyDictionary<string, object> parametrosSalida)
        {
            var output = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var result = this.Execute<int>(ExecuteType.NonQuery, parametrosEntrada, output);
            parametrosSalida = new ReadOnlyDictionary<string, object>(output);
            return result;
        }

        public int EjecutarSinConsultar(ReadOnlyDictionary<string, object> parametrosEntrada)
        {
            return this.Execute<int>(ExecuteType.NonQuery, parametrosEntrada);
        }

        public int ExecuteNonQuery()
        {
            return this.Execute<int>(ExecuteType.NonQuery);
        }

        public IAsyncResult ExecuteNonQueryAsync(ReadOnlyDictionary<string, object> inputParameters, out ReadOnlyDictionary<string, object> outputParameters)
        {
            var output = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var result = this.Execute<IAsyncResult>(ExecuteType.NonQueryAsync, inputParameters, output);
            outputParameters = new ReadOnlyDictionary<string, object>(output);
            return result;
        }

        public IAsyncResult ExecuteNonQueryAsync(ReadOnlyDictionary<string, object> inputParameters)
        {
            return this.Execute<IAsyncResult>(ExecuteType.NonQueryAsync);
        }

        public IAsyncResult ExecuteNonQueryAsync()
        {
            return this.Execute<IAsyncResult>(ExecuteType.NonQueryAsync);
        }

        public int EndExecuteNonQueryAsync(IAsyncResult asyncResult)
        {
            return this.commandAsync.EndExecuteNonQuery(asyncResult);
        }

        public object ExecuteScalar(ReadOnlyDictionary<string, object> inputParameters, out ReadOnlyDictionary<string, object> outputParameters)
        {
            var output = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var result = this.Execute<object>(ExecuteType.Scalar, inputParameters, output);
            outputParameters = new ReadOnlyDictionary<string, object>(output);
            return result;
        }

        public object ExecuteScalar(ReadOnlyDictionary<string, object> inputParameters)
        {
            return this.Execute<object>(ExecuteType.Scalar, inputParameters);
        }

        public object ExecuteScalar()
        {
            return this.Execute<object>(ExecuteType.Scalar);
        }

        public ReadOnlyDictionary<int, DataTable> ObtenerTablas(ReadOnlyDictionary<string, object> parametrosEntrada,
            out ReadOnlyDictionary<string, object> parametrosSalida)
        {
            var output = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var result = this.Execute<ReadOnlyDictionary<int, DataTable>>(ExecuteType.Tables, parametrosEntrada, output);
            parametrosSalida = new ReadOnlyDictionary<string, object>(output);
            return result;
        }

        public ReadOnlyDictionary<int, DataTable> ObtenerTablas(ReadOnlyDictionary<string, object> parametrosEntrada)
        {
            ReadOnlyDictionary<string, object> outputParameters;
            return this.ObtenerTablas(parametrosEntrada, out outputParameters);
        }

        public ReadOnlyDictionary<int, DataTable> ObtenerTablas()
        {
            return this.ObtenerTablas(new Dictionary<string, object>().ADiccionarioSoloLectura());
        }

        public void Dispose()
        {
            if (command != null)
            {
                command.Connection.Close();
                command.Dispose();
            }
            if (commandAsync != null)
            {
                commandAsync.Connection.Close();
                commandAsync.Dispose();
            }
            GC.SuppressFinalize(this);
        }

        public object Clone()
        {
            var result = (Command)MemberwiseClone();
            result.command = NewCommand();
            result.parameterList = Extension.Clonar(parameterList);
            return result;
        }

        private SqlCommand NewCommand()
        {
            return new SqlCommand(Name, new SqlConnection(connectionString))
            {
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = Timeout
            };
        }

        private void CreateCommand(IEnumerable<KeyValuePair<string, object>> inputParameters = null)
        {
            if (command == null)
            {
                command = NewCommand();
            }

            if (inputParameters != null)
            {
                parameterList.ResetParameters();
                foreach (var dbParameter in inputParameters)
                {
                    parameterList[dbParameter.Key].Value = dbParameter.Value;
                }
            }
            if (parameterList != null)
            {
                command.Parameters.Clear();
                foreach (var dbParameter in parameterList
                    .Select(parameter => new SqlParameter(parameter.Name, parameter.Value)
                    {
                        Direction = parameter.Direction,
                        SqlDbType = parameter.Type,
                        Size = parameter.Size
                    }))
                {
                    command.Parameters.Add(dbParameter);
                }
            }

            command.Connection.Open();
        }

        private void SetParameters()
        {
            CreateCommand();
            try
            {
                SqlCommandBuilder.DeriveParameters(command);
                parameterList = new ParameterList();
                foreach (SqlParameter parameter in command.Parameters)
                {
                    parameterList.Add(new Parameter(parameter.ParameterName, parameter.Value, parameter.Direction, parameter.SqlDbType, parameter.Size));
                }
            }
            finally
            {
                command.Connection.Close();
            }
        }

        private void SetOuputParameters(SqlParameterCollection inputParameters, Dictionary<string, object> ouputParameters = null)
        {
            foreach (SqlParameter parameter in inputParameters)
            {
                if (parameter.Direction != ParameterDirection.Input)
                {
                    parameterList[parameter.ParameterName].Value = parameter.Value;
                    if (ouputParameters != null)
                    {
                        ouputParameters.Add(parameter.ParameterName.Replace("@", ""), parameter.Value);
                    }
                }
            }
        }

        private T Execute<T>(ExecuteType action, ReadOnlyDictionary<string, object> inputParameters = null, Dictionary<string, object> ouputParameters = null)
        {
            Exception exception = new ApplicationException("Exception on Command Execute");
            int retry = 1;
            do
            {
                try
                {
                    CreateCommand(inputParameters);
                    object result;
                    switch (action)
                    {
                        case ExecuteType.Reader:
                            {
                                result = command.ExecuteReader(CommandBehavior.CloseConnection);
                                break;
                            }
                        case ExecuteType.NonQuery:
                            {
                                result = command.ExecuteNonQuery();
                                break;
                            }
                        case ExecuteType.NonQueryAsync:
                            {
                                result = command.BeginExecuteNonQuery();
                                commandAsync = command;
                                break;
                            }
                        case ExecuteType.Scalar:
                            {
                                result = command.ExecuteScalar();
                                break;
                            }
                        case ExecuteType.Table:
                            {
                                var dataTable = new DataTable();
                                dataTable.Load(command.ExecuteReader(CommandBehavior.CloseConnection));
                                result = dataTable;
                                break;
                            }
                        case ExecuteType.Tables:
                            {
                                int tableId = 1;
                                var dataTables = new Dictionary<int, DataTable>();
                                using (var reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                                {
                                    do
                                    {
                                        var dataTable = new DataTable();
                                        dataTable.Load(reader);
                                        dataTables.Add(tableId++, dataTable);
                                    } while (!reader.IsClosed);
                                }
                                result = dataTables.ADiccionarioSoloLectura();
                                break;
                            }
                        default:
                            {
                                throw exception;
                            }
                    }
                    SetOuputParameters(command.Parameters, ouputParameters);
                    return (T)result;
                }
                catch (Exception ex)
                {
                    exception = ex;
                    if (retry >= RetryCount)
                    {
                        throw;
                    }
                    retry++;
                    System.Threading.Thread.Sleep(RetryDelay);
                }
                finally
                {
                    if (action != ExecuteType.Reader &&
                        action != ExecuteType.ReaderAsync &&
                        action != ExecuteType.NonQueryAsync)
                    {
                        command.Connection.Close();
                    }
                }
            } while (retry < RetryCount);
            throw exception;
        }
    }
}
