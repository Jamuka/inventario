﻿using System;
using System.Data;

namespace Inventario.Libreria.MsSql
{
    [Serializable]
    public class Parameter
    {
        public Parameter(string name, object value)
            : this(new System.Data.SqlClient.SqlParameter(name, value))
        {
        }

        public Parameter(System.Data.SqlClient.SqlParameter parameter)
            : this(parameter.ParameterName, parameter.Value, parameter.Direction, parameter.SqlDbType)
        {
        }

        public Parameter(string name, object value, ParameterDirection direction, SqlDbType type, int size = -1)
        {
            this.Name = name.Contains("@") ? name : "@" + name;
            this.Value = value;
            this.Direction = direction;
            this.Type = type;
            this.Size = size;
        }

        public string Name
        {
            get;
            private set;
        }

        public object Value
        {
            get;
            set;
        }

        public ParameterDirection Direction
        {
            get;
            set;
        }

        public SqlDbType Type
        {
            get;
            set;
        }

        public int Size
        {
            get;
            set;
        }

        public override string ToString()
        {
            return string.Format("Parameter.{0} = {1};", this.Name, this.Value ?? "NULL");
        }
    }
}
