﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Inventario.Libreria.MsSql
{
    [Serializable]
    public class ParameterList : IList<Parameter>
    {
        private readonly object sync = new object();
        private List<Parameter> dbParameters;

        public ParameterList()
        {
            dbParameters = new List<Parameter>();
        }

        public Parameter this[int index]
        {
            get
            {
                return dbParameters[index];
            }
            set
            {
                dbParameters[index] = value;
            }
        }

        public Parameter this[string name]
        {
            get
            {
                if (!name.StartsWith("@"))
                {
                    name = "@" + name;
                }

                var par = dbParameters.Find(match => match.Name.ToUpper().Equals(name.ToUpper()));
                if (par == null)
                    throw new ArgumentException("Parameter not found on command parameters.", name);
                return par;
            }
        }

        public void Set(IEnumerable<Parameter> parameters)
        {
            lock (sync)
            {
                dbParameters = parameters.ToList();
            }
        }

        public void ResetParameters()
        {
            lock (sync)
            {
                dbParameters.ForEach(dbParameter => dbParameter.Value = null);
            }
        }

        public int IndexOf(Parameter item)
        {
            return dbParameters.IndexOf(item);
        }

        public void Insert(int index, Parameter item)
        {
            dbParameters.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            dbParameters.RemoveAt(index);
        }

        public void Add(Parameter item)
        {
            dbParameters.Add(item);
        }

        public void Clear()
        {
            dbParameters.Clear();
        }

        public bool Contains(Parameter item)
        {
            return dbParameters.Contains(item);
        }

        public void CopyTo(Parameter[] array, int arrayIndex)
        {
            dbParameters.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get
            {
                return dbParameters.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public bool Remove(Parameter item)
        {
            return dbParameters.Remove(item);
        }

        public IEnumerator<Parameter> GetEnumerator()
        {
            return dbParameters.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
