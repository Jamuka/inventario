﻿using System.Collections.Generic;
using Inventario.Comun.Inventario;
using Inventario.Comun.Seguridad;
using Inventario.Comun.Ubicacion;

namespace Inventario.Servicio.Contracts
{
    public interface IAccesoDatos : IServicio
    {
        // ----------------------------- Inventario.Caracteristica
        IEnumerable<ModeloCaracteristica> CaracteristicaObtenerTodos();

        void CaracteristicaGuardar(ModeloCaracteristica modelo);

        void CaracteristicaEliminar(int idCaracteristica);

        // ----------------------------- Inventario.Estado
        IEnumerable<ModeloEstado> EstadoObtenerTodos();

        void EstadoGuardar(ModeloEstado modelo);

        // ----------------------------- Inventario.Historia
        IEnumerable<ModeloHistoria> HistoriaObtenerTodos(int? idSucursal);

        void HistoriaGuardar(ModeloHistoria modelo);

        // ----------------------------- Inventario.Equipo
        ModeloEquipo EquipoObtener(int idEquipo);

        IEnumerable<ModeloEquipo> EquipoObtenerTodos(int? idSucursal, int? idUsuario);

        void EquipoGuardar(ModeloEquipo modelo, int idUsuarioCaptura);

        IEnumerable<ModeloEquipo> EquipoRegistroObtenerTodos(int idEquipo);

        // ----------------------------- Inventario.Tipo
        IEnumerable<ModeloTipo> TipoObtenerTodos();

        void TipoGuardar(ModeloTipo modelo);

        ModeloTipo TipoCaracteristicaObtenerTodos(int idTipo);

        void TipoCaracteristicaGuardar(ModeloTipo modelo);

        // ----------------------------- Seguridad.Permiso
        IEnumerable<ModeloPermiso> PermisoObtenerTodos();

        void PermisoGuardar(ModeloPermiso modelo);

        // ----------------------------- Seguridad.Rol
        IEnumerable<ModeloRol> RolObtenerTodos();

        void RolGuardar(ModeloRol modelo);

        ModeloRol RolPermisoObtenerTodos(int idRol);

        void RolPermisoGuardar(ModeloRol modelo);

        // ----------------------------- Seguridad.Usuario
        IEnumerable<ModeloUsuario> UsuarioObtenerTodos(int? idSucursal);

        ModeloUsuario UsuarioObtener(string identificacion);

        void UsuarioGuardar(ModeloUsuario modelo);

        void UsuarioEliminar(int idUsuario);

        void UsuarioEntrar(int idUsuario, string idSesion, string direccionIp);

        void UsuarioSalir(int idUsuario, string idSesion);

        ModeloUsuario UsuarioRolObtenerTodos(int idUsuario);

        void UsuarioRolGuardar(ModeloUsuario modelo);

        // ----------------------------- Ubicacion.Empresa
        IEnumerable<ModeloEmpresa> EmpresaObtenerTodos();

        void EmpresaGuardar(ModeloEmpresa modelo);

        // ----------------------------- Ubicacion.Sucursal
        IEnumerable<ModeloSucursal> SucursalObtenerTodos();

        void SucursalGuardar(ModeloSucursal modelo);
    }
}
