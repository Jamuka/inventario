﻿using Microsoft.AspNetCore.Http;

namespace Inventario.Servicio.Contracts
{
    public interface IServicioTablero
    {
        void ProcesarArchivo(int idUsuarioCaptura, IFormFile archivo);
    }
}
