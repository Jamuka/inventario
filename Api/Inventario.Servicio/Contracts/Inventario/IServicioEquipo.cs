﻿using System.Collections.Generic;
using Inventario.Comun.Inventario;

namespace Inventario.Servicio.Contracts.Inventario
{
    public interface IServicioEquipo
    {
        ModeloEquipo Obtener(int idEquipo);

        IEnumerable<ModeloEquipo> ObtenerTodos(int? idSucursal, int? idUsuario);

        void Guardar(ModeloEquipo modelo, int idUsuarioCaptura);

        IEnumerable<ModeloEquipo> RegistroObtenerTodos(int idEquipo);
    }
}
