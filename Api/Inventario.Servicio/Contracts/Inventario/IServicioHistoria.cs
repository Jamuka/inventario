﻿using System.Collections.Generic;
using Inventario.Comun.Inventario;

namespace Inventario.Servicio.Contracts.Inventario
{
    public interface IServicioHistoria
    {
        IEnumerable<ModeloHistoria> ObtenerTodos(int? idSucursal);

        void Guardar(ModeloHistoria modelo);
    }
}
