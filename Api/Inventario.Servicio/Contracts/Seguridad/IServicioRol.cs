﻿using System.Collections.Generic;
using Inventario.Comun.Seguridad;

namespace Inventario.Servicio.Contracts.Seguridad
{
    public interface IServicioRol
    {
        IEnumerable<ModeloRol> ObtenerTodos();

        void Guardar(ModeloRol modelo);

        ModeloRol PermisoObtenerTodos(int idRol);

        void PermisoGuardar(ModeloRol modelo);
    }
}