﻿using System.Collections.Generic;
using Inventario.Comun.Ubicacion;

namespace Inventario.Servicio.Contracts.Ubicacion
{
    public interface IServicioEmpresa
    {
        IEnumerable<ModeloEmpresa> ObtenerTodos();

        void Guardar(ModeloEmpresa modelo);
    }
}
