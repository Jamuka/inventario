﻿using System;
using System.Collections.Generic;
using Inventario.Comun.Inventario;
using Inventario.Servicio.Contracts.Inventario;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Inventario.Servicio.Controllers.Inventario
{
    [Authorize, Route("api/inventario/[controller]"), ApiController]
    public class EstadoController : ControllerBase
    {
        private readonly IServicioEstado servicioEstado;

        public EstadoController(IServicioEstado servicioEstado)
        {
            this.servicioEstado = servicioEstado;
        }

        [HttpGet, Route("obtenertodos")]
        public ActionResult<IEnumerable<ModeloEstado>> ObtenerTodos()
        {
            try
            {
                return Ok(this.servicioEstado.ObtenerTodos());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost, Route("guardar")]
        public ActionResult Guardar(ModeloEstado modelo)
        {
            try
            {
                this.servicioEstado.Guardar(modelo);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
