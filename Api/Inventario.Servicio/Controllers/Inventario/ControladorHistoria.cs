﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inventario.Comun.Inventario;
using Inventario.Servicio.Contracts.Inventario;
using Inventario.Servicio.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Inventario.Servicio.Controllers.Inventario
{
    [Authorize, Route("api/inventario/[controller]"), ApiController]
    public class HistoriaController : ControllerBase
    {
        private readonly IServicioHistoria servicioHistoria;

        public HistoriaController(IServicioHistoria servicioHistoria)
        {
            this.servicioHistoria = servicioHistoria;
        }

        [HttpGet, Route("obtenertodos")]
        public ActionResult<IEnumerable<ModeloHistoria>> ObtenerTodos()
        {
            try
            {
                int? idSucursal = null;
                var idSucursalClaim = User.Claims.FirstOrDefault(c => c.Type == Constantes.IdSucursal);
                if (idSucursalClaim != null)
                {
                    if(!string.IsNullOrEmpty(idSucursalClaim.Value))
                    {
                        idSucursal = int.Parse(idSucursalClaim.Value);
                    }
                    else
                    {
                        idSucursal = null;
                    }
                }

                return Ok(this.servicioHistoria.ObtenerTodos(idSucursal));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost, Route("guardar")]
        public ActionResult Guardar(ModeloHistoria modelo)
        {
            try
            {
                this.servicioHistoria.Guardar(modelo);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
