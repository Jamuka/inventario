﻿using System;
using System.Collections.Generic;
using Inventario.Comun.Inventario;
using Inventario.Servicio.Contracts.Inventario;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Inventario.Servicio.Controllers.Inventario
{
    [Authorize, Route("api/inventario/[controller]"), ApiController]
    public class TipoController : ControllerBase
    {
        private readonly IServicioTipo servicioTipo;

        public TipoController(IServicioTipo servicioTipo)
        {
            this.servicioTipo = servicioTipo;
        }

        [HttpGet, Route("obtenertodos")]
        public ActionResult<IEnumerable<ModeloTipo>> ObtenerTodos()
        {
            try
            {
                return Ok(this.servicioTipo.ObtenerTodos());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost, Route("guardar")]
        public ActionResult Guardar(ModeloTipo modelo)
        {
            try
            {
                this.servicioTipo.Guardar(modelo);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet, Route("caracteristicaobtenertodos")]
        public ActionResult<ModeloTipo> Caracteristicaobtenertodos(int idTipo)
        {
            try
            {
                return Ok(this.servicioTipo.CaracteristicaObtenerTodos(idTipo));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost, Route("caracteristicaguardar")]
        public ActionResult CaracteristicaGuardar(ModeloTipo modelo)
        {
            try
            {
                this.servicioTipo.CaracteristicaGuardar(modelo);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
