﻿using System;
using System.Collections.Generic;
using Inventario.Comun.Seguridad;
using Inventario.Servicio.Contracts.Seguridad;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Inventario.Servicio.Controllers.Seguridad
{
    [Authorize, Route("api/seguridad/[controller]"), ApiController]
    public class PermisoController : ControllerBase
    {
        private readonly IServicioPermiso servicioPermiso;

        public PermisoController(IServicioPermiso servicioPermiso)
        {
            this.servicioPermiso = servicioPermiso;
        }

        [HttpGet, Route("obtenertodos")]
        public ActionResult<IEnumerable<ModeloPermiso>> ObtenerTodos()
        {
            try
            {
                return Ok(this.servicioPermiso.ObtenerTodos());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost, Route("guardar")]
        public ActionResult Guardar(ModeloPermiso modelo)
        {
            try
            {
                this.servicioPermiso.Guardar(modelo);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
