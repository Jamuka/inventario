﻿using System;
using System.Collections.Generic;
using Inventario.Comun.Ubicacion;
using Inventario.Servicio.Contracts.Ubicacion;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Inventario.Servicio.Controllers.Ubicacion
{
    [Route("api/ubicacion/[controller]"), ApiController]
    public class SucursalController : ControllerBase
    {
        private readonly IServicioSucursal servicioSucursal;

        public SucursalController(IServicioSucursal servicioSucursal)
        {
            this.servicioSucursal = servicioSucursal;
        }

        [HttpGet, Route("obtenertodos")]
        public ActionResult<IEnumerable<ModeloSucursal>> ObtenerTodos()
        {
            try
            {
                return Ok(this.servicioSucursal.ObtenerTodos());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize, HttpPost, Route("guardar")]
        public ActionResult Guardar(ModeloSucursal modelo)
        {
            try
            {
                this.servicioSucursal.Guardar(modelo);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
