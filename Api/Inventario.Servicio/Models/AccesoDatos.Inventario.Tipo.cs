﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Inventario.Comun.Inventario;
using Inventario.Libreria;
using Inventario.Servicio.Models.TipoTabla;

namespace Inventario.Servicio.Models
{
    public partial class AccesoDatos
    {
        internal partial class ProcedimientosAlmacenados
        {
            internal const string TipoObtenerTodos = "[Inventario].[TipoObtenerTodos]";
            internal const string TipoGuardar = "[Inventario].[TipoGuardar]";
            internal const string TipoCaracteristicaObtenerTodos = "[Inventario].[TipoCaracteristicaObtenerTodos]";
            internal const string TipoCaracteristicaGuardar = "[Inventario].[TipoCaracteristicaGuardar]";

            internal partial class Campos
            {
                internal const string IdTipo = "IdTipo";
                internal const string Caracteristicas = "Caracteristicas";
            }
        }

        public IEnumerable<ModeloTipo> TipoObtenerTodos()
        {
            List<ModeloTipo> valorRetorno = null;

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.TipoObtenerTodos))
            {
                var tablas = comando.ObtenerTablas();
                if (tablas.Count == 2)
                {
                    var tablaTipos = tablas[1];
                    var tablaCaracteristicas = tablas[2];

                    if (tablaTipos.Rows.Count > 0)
                    {
                        valorRetorno = new List<ModeloTipo>();
                        for (var i = 0; i < tablaTipos.Rows.Count; i++)
                        {
                            valorRetorno.Add(this.RegistroAModeloTipo(tablaTipos.Rows[i]));
                        }

                        if(tablaCaracteristicas.Rows.Count > 0)
                        {
                            for(var i = 0; i < tablaCaracteristicas.Rows.Count; i++)
                            {
                                if (tablaCaracteristicas.Rows[i][ProcedimientosAlmacenados.Campos.IdTipo] != DBNull.Value)
                                {
                                    var idTipo = this.ObtenerValorCampo<int>(tablaCaracteristicas.Rows[i], ProcedimientosAlmacenados.Campos.IdTipo);
                                    var tipo = valorRetorno.FirstOrDefault(t => t.IdTipo == idTipo);
                                    if (tipo != null)
                                    {
                                        if (tipo.Caracteristicas == null)
                                        {
                                            tipo.Caracteristicas = new List<ModeloCaracteristica>();
                                        }
                                        tipo.Caracteristicas.Add(this.RegistroAModeloCaracteristica(tablaCaracteristicas.Rows[i]));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return valorRetorno;
        }

        public void TipoGuardar(ModeloTipo modelo)
        {
            var parametros = this.ModeloCatalogoADiccionario(modelo);
            parametros.Add(ProcedimientosAlmacenados.Campos.IdTipo, modelo.IdTipo);

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.TipoGuardar))
            {
                comando.EjecutarSinConsultar(parametros.ADiccionarioSoloLectura());
            }
        }

        public ModeloTipo TipoCaracteristicaObtenerTodos(int idTipo)
        {
            ModeloTipo valorRetorno = null;
            var parametros = new Dictionary<string, object>
            {
                { ProcedimientosAlmacenados.Campos.IdTipo, idTipo }
            };

            using (var comando = this.ObtenerComando(ProcedimientosAlmacenados.TipoCaracteristicaObtenerTodos))
            {
                var tablas = comando.ObtenerTablas(parametros.ADiccionarioSoloLectura());

                if (tablas.Count == 2)
                {
                    var tablaTipo = tablas[1];
                    var tablaCaracteristica = tablas[2];

                    if (tablaTipo.Rows.Count > 0)
                    {
                        valorRetorno = this.RegistroAModeloTipo(tablaTipo.Rows[0]);
                    }

                    if (tablaCaracteristica.Rows.Count > 0)
                    {
                        valorRetorno.Caracteristicas = new List<ModeloCaracteristica>();
                        for (var i = 0; i < tablaCaracteristica.Rows.Count; i++)
                        {
                            var registro = tablaCaracteristica.Rows[i];

                            valorRetorno.Caracteristicas.Add(this.RegistroAModeloCaracteristica(registro));
                        }
                    }
                }
            }
            return valorRetorno;
        }

        public void TipoCaracteristicaGuardar(ModeloTipo modelo)
        {
            using (var caracteristicas = new TTipoCaracteristica())
            {
                caracteristicas.Add(modelo.IdTipo, modelo.Caracteristicas);
                var parametros = new Dictionary<string, object>
                {
                    { ProcedimientosAlmacenados.Campos.Caracteristicas, caracteristicas }
                };

                using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.TipoCaracteristicaGuardar))
                {
                    comando.EjecutarSinConsultar(parametros.ADiccionarioSoloLectura());
                }
            }
        }

        private ModeloTipo RegistroAModeloTipo(DataRow registro)
        {
            var modeloCatalogo = this.RegistroAModeloCatalogo(registro);
            var valorRetorno = new ModeloTipo
            {
                Nombre = modeloCatalogo.Nombre,
                Activo = modeloCatalogo.Activo,
                FechaInsercion = modeloCatalogo.FechaInsercion,
                FechaActualizacion = modeloCatalogo.FechaActualizacion
            };

            valorRetorno.IdTipo = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdTipo);
            return valorRetorno;
        }
    }
}