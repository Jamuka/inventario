﻿using System.Collections.Generic;
using System.Data;
using Inventario.Comun.Ubicacion;
using Inventario.Libreria;

namespace Inventario.Servicio.Models
{
    public partial class AccesoDatos
    {
        internal partial class ProcedimientosAlmacenados
        {
            internal const string EmpresaObtenerTodos = "[Ubicacion].[EmpresaObtenerTodos]";
            internal const string EmpresaGuardar = "[Ubicacion].[EmpresaGuardar]";

            internal partial class Campos
            {
                internal const string IdEmpresa = "IdEmpresa";
                internal const string UrlLogo = "UrlLogo";
            }
        }

        public IEnumerable<ModeloEmpresa> EmpresaObtenerTodos()
        {
            List<ModeloEmpresa> valorRetorno = null;

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.EmpresaObtenerTodos))
            {
                var tabla = comando.ObtenerTabla();

                if (tabla.Rows.Count > 0)
                {
                    valorRetorno = new List<ModeloEmpresa>();
                    for (var i = 0; i < tabla.Rows.Count; i++)
                    {
                        var registro = tabla.Rows[i];

                        valorRetorno.Add(this.RegistroAModeloEmpresa(registro));
                    }
                }
            }
            return valorRetorno;
        }

        public void EmpresaGuardar(ModeloEmpresa modelo)
        {
            var parametros = this.ModeloCatalogoADiccionario(modelo);
            parametros.Add(ProcedimientosAlmacenados.Campos.IdEmpresa, modelo.IdEmpresa);
            parametros.Add(ProcedimientosAlmacenados.Campos.UrlLogo, modelo.UrlLogo);

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.EmpresaGuardar))
            {
                comando.EjecutarSinConsultar(parametros.ADiccionarioSoloLectura());
            }
        }

        private ModeloEmpresa RegistroAModeloEmpresa(DataRow registro)
        {
            var modeloCatalogo = this.RegistroAModeloCatalogo(registro);
            var valorRetorno = new ModeloEmpresa
            {
                Nombre = modeloCatalogo.Nombre,
                Activo = modeloCatalogo.Activo,
                FechaInsercion = modeloCatalogo.FechaInsercion,
                FechaActualizacion = modeloCatalogo.FechaActualizacion
            };

            valorRetorno.IdEmpresa = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdEmpresa);
            valorRetorno.UrlLogo = this.ObtenerValorCampo<string>(registro, ProcedimientosAlmacenados.Campos.UrlLogo);
            return valorRetorno;
        }
    }
}
