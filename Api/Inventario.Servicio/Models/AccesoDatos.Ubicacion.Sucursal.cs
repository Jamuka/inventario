﻿using System;
using System.Collections.Generic;
using System.Data;
using Inventario.Comun.Ubicacion;
using Inventario.Libreria;

namespace Inventario.Servicio.Models
{
    public partial class AccesoDatos
    {
        internal partial class ProcedimientosAlmacenados
        {
            internal const string SucursalObtenerTodos = "[Ubicacion].[SucursalObtenerTodos]";
            internal const string SucursalGuardar = "[Ubicacion].[SucursalGuardar]";

            internal partial class Campos
            {
                internal const string IdSucursal = "IdSucursal";
            }
        }

        public IEnumerable<ModeloSucursal> SucursalObtenerTodos()
        {
            List<ModeloSucursal> valorRetorno = null;

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.SucursalObtenerTodos))
            {
                var tabla = comando.ObtenerTabla();

                if (tabla.Rows.Count > 0)
                {
                    valorRetorno = new List<ModeloSucursal>();
                    for (var i = 0; i < tabla.Rows.Count; i++)
                    {
                        var registro = tabla.Rows[i];

                        valorRetorno.Add(this.RegistroAModeloSucursal(registro));
                    }
                }
            }
            return valorRetorno;
        }

        public void SucursalGuardar(ModeloSucursal modelo)
        {
            var parametros = this.ModeloCatalogoADiccionario(modelo);
            parametros.Add(ProcedimientosAlmacenados.Campos.IdSucursal, modelo.IdSucursal);
            parametros.Add(ProcedimientosAlmacenados.Campos.IdEmpresa, modelo.IdEmpresa);

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.SucursalGuardar))
            {
                comando.EjecutarSinConsultar(parametros.ADiccionarioSoloLectura());
            }
        }

        private ModeloSucursal RegistroAModeloSucursal(DataRow registro)
        {
            var modeloCatalogo = this.RegistroAModeloCatalogo(registro);
            var valorRetorno = new ModeloSucursal
            {
                Nombre = modeloCatalogo.Nombre,
                Activo = modeloCatalogo.Activo,
                FechaInsercion = modeloCatalogo.FechaInsercion,
                FechaActualizacion = modeloCatalogo.FechaActualizacion
            };

            valorRetorno.IdSucursal = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdSucursal);
            valorRetorno.IdEmpresa = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdEmpresa);
            return valorRetorno;
        }
    }
}
