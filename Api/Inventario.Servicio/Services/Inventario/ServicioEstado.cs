﻿using System.Collections.Generic;
using Inventario.Comun.Inventario;
using Inventario.Servicio.Contracts;
using Inventario.Servicio.Contracts.Inventario;

namespace Inventario.Servicio.Services.Inventario
{
    public class ServicioEstado : Servicio, IServicioEstado
    {
        private readonly IAccesoDatos accesoDatos;

        public ServicioEstado(IAccesoDatos accesoDatos)
        {
            this.accesoDatos = accesoDatos;
        }

        public void Guardar(ModeloEstado modelo)
        {
            this.accesoDatos.EstadoGuardar(modelo);
        }

        public IEnumerable<ModeloEstado> ObtenerTodos()
        {
            var valorRetorno = this.accesoDatos.EstadoObtenerTodos();

            return valorRetorno;
        }
    }
}