﻿using System.Collections.Generic;
using Inventario.Comun.Inventario;
using Inventario.Servicio.Contracts;
using Inventario.Servicio.Contracts.Inventario;

namespace Inventario.Servicio.Services.Inventario
{
    public class ServicioHistoria : Servicio, IServicioHistoria
    {
        private readonly IAccesoDatos accesoDatos;

        public ServicioHistoria(IAccesoDatos accesoDatos)
        {
            this.accesoDatos = accesoDatos;
        }

        public void Guardar(ModeloHistoria modelo)
        {
            this.accesoDatos.HistoriaGuardar(modelo);
        }

        public IEnumerable<ModeloHistoria> ObtenerTodos(int? idSucursal)
        {
            var valorRetorno = this.accesoDatos.HistoriaObtenerTodos(idSucursal);

            return valorRetorno;
        }
    }
}