﻿using System.Collections.Generic;
using Inventario.Comun.Seguridad;
using Inventario.Servicio.Contracts;
using Inventario.Servicio.Contracts.Seguridad;

namespace Inventario.Servicio.Services.Seguridad
{
    public class ServicioRol : Servicio, IServicioRol
    {
        private readonly IAccesoDatos accesoDatos;

        public ServicioRol(IAccesoDatos accesoDatos)
        {
            this.accesoDatos = accesoDatos;
        }

        public void Guardar(ModeloRol modelo)
        {
            this.accesoDatos.RolGuardar(modelo);
        }

        public IEnumerable<ModeloRol> ObtenerTodos()
        {
            var valorRetorno = this.accesoDatos.RolObtenerTodos();

            return valorRetorno;
        }

        public void PermisoGuardar(ModeloRol modelo)
        {
            this.accesoDatos.RolPermisoGuardar(modelo);
        }

        public ModeloRol PermisoObtenerTodos(int idRol)
        {
            var valorRetorno = this.accesoDatos.RolPermisoObtenerTodos(idRol);

            return valorRetorno;
        }
    }
}