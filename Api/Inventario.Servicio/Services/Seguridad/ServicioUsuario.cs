﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Inventario.Comun.Seguridad;
using Inventario.Servicio.Contracts;
using Inventario.Servicio.Contracts.Seguridad;
using Inventario.Servicio.Models;
using Microsoft.IdentityModel.Tokens;

namespace Inventario.Servicio.Services.Seguridad
{
    public class ServicioUsuario : Servicio, IServicioUsuario
    {
        private readonly IAccesoDatos accesoDatos;

        public ServicioUsuario(IAccesoDatos accesoDatos)
        {
            this.accesoDatos = accesoDatos;
        }

        public ModeloRespuestaEntrar Entrar(ModeloSolicitudEntrar modelo)
        {
            var usuario = this.accesoDatos.UsuarioObtener(modelo.Identificacion);

            ModeloRespuestaEntrar returnValue = null;

            if (usuario == null)
            {
                throw new Exception("Excepciones.UsuarioNoExiste");
            }
            else
            {
                returnValue = new ModeloRespuestaEntrar { Usuario = usuario };
                if (returnValue.Usuario.Contrasena != modelo.Contrasena)
                {
                    throw new Exception("Excepciones.ContrasenaInvalida");
                }
                else
                {
                    var idSucursal = modelo.IdSucursal.HasValue ? modelo.IdSucursal : usuario.IdSucursal;

                    if (idSucursal.HasValue)
                    {
                        returnValue.Sucursal = this.accesoDatos.SucursalObtenerTodos().FirstOrDefault(s => s.IdSucursal == idSucursal);
                        returnValue.Empresa = this.accesoDatos.EmpresaObtenerTodos().FirstOrDefault(e => e.IdEmpresa == returnValue.Sucursal.IdEmpresa);
                    }
                    var key = Encoding.UTF8.GetBytes(Constructor.Instancia.ObtenerConfiguracion().ClaveSecreta);
                    var idSesion = Guid.NewGuid().ToString();

                    var seconds = 1800;
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                            new Claim(Constantes.IdUsuario, returnValue.Usuario.IdUsuario.ToString()),
                            new Claim(Constantes.IdSesion, idSesion),
                            new Claim(Constantes.IdSucursal, idSucursal.ToString()),
                        }),
                        Expires = DateTime.UtcNow.AddSeconds(seconds),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                    };

                    var tokenHandler = new JwtSecurityTokenHandler();
                    var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                    returnValue.Llave = tokenHandler.WriteToken(securityToken);
                    returnValue.Usuario.Contrasena = string.Empty;
                    Task.Factory.StartNew(() => this.accesoDatos.UsuarioEntrar(returnValue.Usuario.IdUsuario, idSesion, modelo.DireccionIp));
                }
            }

            return returnValue;
        }

        public void Guardar(ModeloUsuario modelo)
        {
            this.accesoDatos.UsuarioGuardar(modelo);
        }

        public void Eliminar(int idUsuario)
        {
            this.accesoDatos.UsuarioEliminar(idUsuario);
        }

        public IEnumerable<ModeloUsuario> ObtenerTodos(int? idSucursal)
        {
            var valorRetorno = this.accesoDatos.UsuarioObtenerTodos(idSucursal);

            return valorRetorno;
        }

        public IEnumerable<ModeloUsuario> ObtenerTodosPorRol(string rol)
        {
            var usuarios = this.ObtenerTodos(null);
            var valorRetorno = usuarios.Where(u => u.Roles.Any(r => r.Nombre == rol));

            if (valorRetorno != null)
            {
                foreach (var usuario in valorRetorno)
                {
                    usuario.Contrasena = string.Empty;
                }
            }
            return valorRetorno;
        }

        public void RolGuardar(ModeloUsuario modelo)
        {
            this.accesoDatos.UsuarioRolGuardar(modelo);
        }

        public ModeloUsuario RolObtenerTodos(int idUsuario)
        {
            var valorRetorno = this.accesoDatos.UsuarioRolObtenerTodos(idUsuario);

            return valorRetorno;
        }

        public void Salir(int idUsuario, string idSesion)
        {
            Task.Factory.StartNew(() => this.accesoDatos.UsuarioSalir(idUsuario, idSesion));
        }
    }
}