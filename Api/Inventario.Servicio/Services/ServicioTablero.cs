﻿using System;
using System.Linq;
using ExcelDataReader;
using Inventario.Servicio.Contracts;
using Microsoft.AspNetCore.Http;

namespace Inventario.Servicio.Services
{
    public class ServicioTablero : Servicio, IServicioTablero
    {
        private readonly IAccesoDatos accesoDatos;

        public ServicioTablero(IAccesoDatos accesoDatos)
        {
            this.accesoDatos = accesoDatos;
        }

        public void ProcesarArchivo(int idUsuarioCaptura, IFormFile archivo)
        {
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            using (var reader = ExcelReaderFactory.CreateReader(archivo.OpenReadStream()))
            {
                reader.Read(); // Encabezados
                var tipos = this.accesoDatos.TipoObtenerTodos();
                var tipo = tipos.FirstOrDefault(t=> t.Nombre == "Equipos comp");
                while (reader.Read()) 
                {                                        
                    // -- Empresa
                    var empresaNombre = reader.GetValue(8).ToString().Trim();
                    var empresas = this.accesoDatos.EmpresaObtenerTodos();
                    if(!empresas.Any(e=> e.Nombre == empresaNombre))
                    {
                        accesoDatos.EmpresaGuardar(new Comun.Ubicacion.ModeloEmpresa
                        {
                            Nombre = empresaNombre
                        });
                    }
                    empresas = this.accesoDatos.EmpresaObtenerTodos();
                    var empresa = empresas.FirstOrDefault(e => e.Nombre == empresaNombre);

                    // -- SucursalUsuario
                    var usuarioSucursalNombre = reader.GetValue(4).ToString().Trim();
                    var sucursales = this.accesoDatos.SucursalObtenerTodos();
                    if(sucursales == null || !sucursales.Any(s=> s.Nombre == usuarioSucursalNombre))
                    {
                        accesoDatos.SucursalGuardar(new Comun.Ubicacion.ModeloSucursal
                        {
                            Nombre = usuarioSucursalNombre,
                            IdEmpresa = empresa.IdEmpresa
                        });
                    }
                    sucursales = this.accesoDatos.SucursalObtenerTodos();
                    var usuarioSucursal = sucursales.FirstOrDefault(s=>s.Nombre == usuarioSucursalNombre);

                    // -- SucursalEquipo
                    var equipoSucursalNombre = reader.GetValue(5).ToString().Trim();
                    if (!sucursales.Any(s => s.Nombre == equipoSucursalNombre))
                    {
                        accesoDatos.SucursalGuardar(new Comun.Ubicacion.ModeloSucursal
                        {
                            Nombre = equipoSucursalNombre,
                            IdEmpresa = empresa.IdEmpresa
                        });
                    }
                    sucursales = this.accesoDatos.SucursalObtenerTodos();
                    var equipoSucursal = sucursales.FirstOrDefault(s => s.Nombre == equipoSucursalNombre);

                    // -- Usuario
                    var usuarioIdentificacion = reader.GetValue(0).ToString().Trim();
                    var usuarios = this.accesoDatos.UsuarioObtenerTodos(null);
                    if (!usuarios.Any(s => s.Identificacion == usuarioIdentificacion))
                    {
                        var usuarioNombre = reader.GetValue(1).ToString().Trim();
                        var usuarioEmail = reader.GetValue(2).ToString().Trim();
                        var usuarioRut = reader.GetValue(3).ToString().Trim();
                        accesoDatos.UsuarioGuardar(new Comun.Seguridad.ModeloUsuario
                        {
                            Identificacion = usuarioIdentificacion,
                            Nombre = usuarioNombre,
                            Correo = usuarioEmail,
                            Rut = usuarioRut,
                            IdSucursal = usuarioSucursal.IdSucursal
                        });
                    }
                    usuarios = this.accesoDatos.UsuarioObtenerTodos(null);
                    var usuario = usuarios.FirstOrDefault(s => s.Identificacion == usuarioIdentificacion);

                    // -- Estado
                    var equipoEstaddo = reader.GetValue(17).ToString().Trim();
                    var estados = this.accesoDatos.EstadoObtenerTodos();
                    if (!estados.Any(e => e.Nombre == equipoEstaddo))
                    {
                        accesoDatos.EstadoGuardar(new Comun.Inventario.ModeloEstado
                        {
                            Nombre = equipoEstaddo
                        });
                    }
                    estados = this.accesoDatos.EstadoObtenerTodos();
                    var estado = estados.FirstOrDefault(e => e.Nombre == equipoEstaddo);

                    // -- Equipo                    
                    var equipoActivoFijo = reader.GetValue(6) != null ? reader.GetValue(6).ToString().Trim() : "";
                    if (!string.IsNullOrEmpty(equipoActivoFijo))
                    {
                        var equipos = this.accesoDatos.EquipoObtenerTodos(null, null);
                        if (equipos==null || !equipos.Any(e => e.ActivoFijo == equipoActivoFijo))
                        {
                            var equipoNombre = reader.GetValue(7) != null ? reader.GetValue(7).ToString().Trim() : "";
                            var equipoMarca = reader.GetValue(9) != null ? reader.GetValue(9).ToString().Trim() : "";
                            var equipoModelo = reader.GetValue(10) !=null ? reader.GetValue(10).ToString().Trim() : "";
                            var equipoSerie = reader.GetValue(11) != null ? reader.GetValue(11).ToString().Trim() : "";
                            var equipoFechaCompra = reader.GetValue(18).ToString().Trim();                            

                            var caracteristicaSistemaOperativo = reader.GetValue(12) !=null ? reader.GetValue(12).ToString().Trim() : "";
                            var caracteristicaTipoAlmacenamiento = reader.GetValue(13) != null ? reader.GetValue(13).ToString().Trim() : "";
                            var caracteristicaTamano = reader.GetValue(14) != null? reader.GetValue(14).ToString().Trim() : "";
                            var caracteristicaMemoria = reader.GetValue(15) != null ? reader.GetValue(15).ToString().Trim() : "";
                            var caracteristicaProcesador = reader.GetValue(16) != null ? reader.GetValue(16).ToString().Trim() : "";
                            var caracteristicas = tipo.Caracteristicas.ToList();

                            if (!string.IsNullOrEmpty(caracteristicaSistemaOperativo))
                            {
                                var sistemaOperativo = caracteristicas.FirstOrDefault(c => c.Nombre == "Sistema Operativo");
                                if (sistemaOperativo != null)
                                {
                                    sistemaOperativo.Valor = caracteristicaSistemaOperativo;
                                }
                            }

                            if (!string.IsNullOrEmpty(caracteristicaTipoAlmacenamiento))
                            {
                                var almacenamiento = caracteristicas.FirstOrDefault(c => c.Nombre == "Almacenamiento");
                                if (almacenamiento != null)
                                {
                                    almacenamiento.Valor = caracteristicaTipoAlmacenamiento;
                                }
                            }

                            if (!string.IsNullOrEmpty(caracteristicaTamano))
                            {
                                var tamano = caracteristicas.FirstOrDefault(c => c.Nombre == "Tamano");
                                if (tamano != null)
                                {
                                    tamano.Valor = caracteristicaTamano;
                                }
                            }

                            if (!string.IsNullOrEmpty(caracteristicaMemoria))
                            {
                                var memoria = caracteristicas.FirstOrDefault(c => c.Nombre == "Memoria");
                                if (memoria != null)
                                {
                                    memoria.Valor = caracteristicaMemoria;
                                }
                            }

                            if (!string.IsNullOrEmpty(caracteristicaProcesador))
                            {
                                var procesador = caracteristicas.FirstOrDefault(c => c.Nombre == "Procesador");
                                if (procesador != null)
                                {
                                    procesador.Valor = caracteristicaProcesador;
                                }
                            }

                            accesoDatos.EquipoGuardar(new Comun.Inventario.ModeloEquipo
                            {
                                Nombre = equipoNombre,
                                ActivoFijo = equipoActivoFijo,
                                Marca = equipoMarca,
                                Modelo = equipoModelo,
                                Serie = equipoSerie,
                                FechaCompra = Convert.ToDateTime(equipoFechaCompra),
                                Caracteristicas = caracteristicas,
                                IdUsuario = usuario.IdUsuario,
                                IdSucursal = equipoSucursal.IdSucursal,
                                IdTipo = tipo.IdTipo,
                                IdEstado = estado.IdEstado,
                                Numero = 0
                            }, idUsuarioCaptura);
                        }
                    }
                }
            }
        }
    }
}
