﻿using System.Collections.Generic;
using System.Linq;
using Inventario.Comun.Ubicacion;
using Inventario.Servicio.Contracts;
using Inventario.Servicio.Contracts.Ubicacion;
using Inventario.Servicio.Models;
using Microsoft.AspNetCore.Hosting;

namespace Inventario.Servicio.Services.Ubicacion
{
    public class ServicioEmpresa : Servicio, IServicioEmpresa
    {
        private readonly IAccesoDatos accesoDatos;
        private readonly IWebHostEnvironment hostingEnvironment;

        public ServicioEmpresa(IAccesoDatos accesoDatos, IWebHostEnvironment hostingEnvironment)
        {
            this.hostingEnvironment = hostingEnvironment;
            this.accesoDatos = accesoDatos;
        }

        public void Guardar(ModeloEmpresa modelo)
        {
            var nombreImagen = string.Format("{0}.png", modelo.Nombre);
            modelo.UrlLogo = Constantes.DirectorioImagen + nombreImagen;

            var empresaVieja = this.accesoDatos.EmpresaObtenerTodos().FirstOrDefault(e=>e.IdEmpresa == modelo.IdEmpresa);
            if (modelo.Imagen == null && empresaVieja != null && !string.IsNullOrEmpty(empresaVieja.UrlLogo))
            {
                ServicioImagen.Guardar(this.hostingEnvironment.ContentRootPath + empresaVieja.UrlLogo, this.hostingEnvironment.ContentRootPath + modelo.UrlLogo);
            }

            this.accesoDatos.EmpresaGuardar(modelo);

            if (modelo.Imagen != null)
            {
                ServicioImagen.Guardar(modelo.Imagen, this.hostingEnvironment.ContentRootPath + Constantes.DirectorioImagen, nombreImagen);
            }
        }

        public IEnumerable<ModeloEmpresa> ObtenerTodos()
        {
            var valorRetorno = this.accesoDatos.EmpresaObtenerTodos();

            return valorRetorno;
        }
    }
}