﻿using System.Collections.Generic;
using Inventario.Comun.Ubicacion;
using Inventario.Servicio.Contracts;
using Inventario.Servicio.Contracts.Ubicacion;

namespace Inventario.Servicio.Services.Ubicacion
{
    public class ServicioSucursal : Servicio, IServicioSucursal
    {
        private readonly IAccesoDatos accesoDatos;

        public ServicioSucursal(IAccesoDatos accesoDatos)
        {
            this.accesoDatos = accesoDatos;
        }

        public void Guardar(ModeloSucursal modelo)
        {
            this.accesoDatos.SucursalGuardar(modelo);
        }

        public IEnumerable<ModeloSucursal> ObtenerTodos()
        {
            var valorRetorno = this.accesoDatos.SucursalObtenerTodos();

            return valorRetorno;
        }
    }
}