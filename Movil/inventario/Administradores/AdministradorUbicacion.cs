﻿using System.Collections.Generic;
using System.Threading.Tasks;
using inventario.Contratos;
using Inventario.Comun.Ubicacion;

namespace inventario.Administradores
{
    public class AdministradorUbicacion
    {
        private IServicioUbicacion servicioUbicacion;

        public AdministradorUbicacion(IServicioUbicacion servicioUbicacion)
        {
            this.servicioUbicacion = servicioUbicacion;
        }

        public List<ModeloSucursal> Sucursales
        {
            get;
            set;
        }

        public Task<List<ModeloSucursal>> SucursalObtenerTodos()
        {
            return this.servicioUbicacion.SucursalObtenerTodos();
        }
    }
}
