﻿namespace inventario.Modelo
{
    public class Constantes
    {
        //public const string DireccionApi = "http://192.168.100.16:33906";
        public const string DireccionApi = "https://inventarioservicio.azurewebsites.net";
        /* -- Inventario -- */
        public const string ApiInventarioEquipo = DireccionApi + "/api/inventario/equipo/";
        public const string ApiInventarioHistoria = DireccionApi + "/api/inventario/historia/";
        public const string ApiInventarioTipo = DireccionApi + "/api/inventario/tipo/";
        public const string ApiInventarioEstado = DireccionApi + "/api/inventario/estado/";
        /* -- Seguridad -- */
        public const string ApiInventarioUsuario = DireccionApi + "/api/seguridad/usuario/";
        /* -- Ubicacion -- */
        public const string ApiInventarioSucursal = DireccionApi + "/api/ubicacion/sucursal/";
    }
}
