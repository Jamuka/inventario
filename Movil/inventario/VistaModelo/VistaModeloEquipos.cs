﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using inventario.Vista;
using Inventario.Comun.Inventario;
using Xamarin.Forms;

namespace inventario.VistaModelo
{
    public class VistaModeloEquipos : INotifyPropertyChanged
    {
        private ModeloEquipo equipoSeleccionado;
        private bool estaActualizando;
        private BackgroundWorker tareaActualizar;
        private int? idUsuario;

        public VistaModeloEquipos()
        {
            this.ListaEquipo = new ObservableCollection<ModeloEquipo>();
            this.ComandoActualizar = new Command(()=>this.Actualizar());
            this.ComandoNuevo = new Command(()=> this.Nuevo());

            this.tareaActualizar = new BackgroundWorker();
            this.tareaActualizar.DoWork += this.TareaActualizarEjecucion;
            this.tareaActualizar.RunWorkerCompleted += this.TareaActualizarTerminado;
            this.tareaActualizar.RunWorkerAsync();
        }

        public VistaModeloEquipos(int? idUsuario) : this()
        {
            this.idUsuario = idUsuario;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<ModeloEquipo> ListaEquipo
        {
            get;
            set;
        }

        public ModeloEquipo EquipoSeleccionado
        {
            get
            {
                return this.equipoSeleccionado;
            }
            set
            {
                this.equipoSeleccionado = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(EquipoSeleccionado)));
                this.AbrirEquipo();
            }
        }

        public bool EstaActualizando
        {
            get
            {
                return this.estaActualizando;
            }
            set
            {
                this.estaActualizando = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(EstaActualizando)));
            }
        }

        public ICommand ComandoActualizar
        {
            get;
            private set;
        }

        public ICommand ComandoNuevo
        {
            get;
            private set;
        }

        private void Actualizar()
        {
            this.tareaActualizar.RunWorkerAsync();            
        }

        private async void Nuevo()
        {
            var vistaModeloEquipo = new VistaModeloEquipo(new ModeloEquipo(), this.idUsuario);
            var vistaEquipo = new VistaEquipo
            {
                BindingContext = vistaModeloEquipo
            };

            await Application.Current.MainPage.Navigation.PushAsync(vistaEquipo);
        }

        private void TareaActualizarEjecucion(object sender, DoWorkEventArgs e)
        {
            System.Diagnostics.Trace.WriteLine("TareaActualizarEjecucion " + System.DateTime.Now.ToLongTimeString());
            Thread.Sleep(200);
            this.EstaActualizando = true;
            this.ListaEquipo.Clear();
            var equiposTask = App.AdministradorInventario.EquipoObtenerTodos(idUsuario);
            var estadosTask = App.AdministradorInventario.EstadoObtenerTodos();
            var tiposTask = App.AdministradorInventario.TipoObtenerTodos();
            var usuariosTask = App.AdministradorSeguridad.UsuarioObtenerTodos();
            var sucursalesTask = App.AdministradorUbicacion.SucursalObtenerTodos();

            Task.WaitAll(equiposTask, estadosTask, tiposTask, usuariosTask, sucursalesTask);

            foreach (var equipo in equiposTask.Result)
            {
                this.ListaEquipo.Add(equipo);
            }

            App.AdministradorInventario.Estados = estadosTask.Result;
            System.Diagnostics.Trace.WriteLine("TareaActualizarEjecucion(Estados) " + System.DateTime.Now.ToLongTimeString());

            App.AdministradorInventario.Tipos = tiposTask.Result;
            System.Diagnostics.Trace.WriteLine("TareaActualizarEjecucion(Tipos) " + System.DateTime.Now.ToLongTimeString());

            App.AdministradorSeguridad.Usuarios = usuariosTask.Result;
            System.Diagnostics.Trace.WriteLine("TareaActualizarEjecucion(Usuarios) " + System.DateTime.Now.ToLongTimeString());

            App.AdministradorUbicacion.Sucursales = sucursalesTask.Result;
            System.Diagnostics.Trace.WriteLine("TareaActualizarEjecucion(Sucursales) " + System.DateTime.Now.ToLongTimeString());
        }

        private void TareaActualizarTerminado(object sender, RunWorkerCompletedEventArgs e)
        {
            this.EstaActualizando = false;
            System.Diagnostics.Trace.WriteLine("TareaActualizarTerminado " + System.DateTime.Now.ToLongTimeString());
        }

        private async void AbrirEquipo()
        {
            var vistaModeloEquipo = new VistaModeloEquipo(this.EquipoSeleccionado, this.idUsuario);
            var vistaEquipo = new VistaEquipo
            {
                BindingContext = vistaModeloEquipo
            };
              
            await Application.Current.MainPage.Navigation.PushAsync(vistaEquipo);
        }
    }
}
