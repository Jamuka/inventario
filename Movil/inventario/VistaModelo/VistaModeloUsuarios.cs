﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using inventario.Vista;
using Inventario.Comun.Seguridad;
using Xamarin.Forms;

namespace inventario.VistaModelo
{
    public class VistaModeloUsuarios : INotifyPropertyChanged
    {
        private ModeloUsuario usuarioSeleccionado;
        private bool estaActualizando;
        private BackgroundWorker tareaActualizar;

        public VistaModeloUsuarios()
        {
            this.ListaUsuario = new ObservableCollection<ModeloUsuario>();
            this.ComandoActualizar = new Command(() => this.Actualizar());

            this.tareaActualizar = new BackgroundWorker();
            this.tareaActualizar.DoWork += this.TareaActualizarEjecucion;
            this.tareaActualizar.RunWorkerCompleted += this.TareaActualizarTerminado;
            this.tareaActualizar.RunWorkerAsync();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<ModeloUsuario> ListaUsuario
        {
            get;
            set;
        }

        public ModeloUsuario UsuarioSeleccionado
        {
            get
            {
                return this.usuarioSeleccionado;
            }
            set
            {
                this.usuarioSeleccionado = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(UsuarioSeleccionado)));
                this.AbrirUsuario();
            }
        }

        public bool EstaActualizando
        {
            get
            {
                return this.estaActualizando;
            }
            set
            {
                this.estaActualizando = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(EstaActualizando)));
            }
        }

        public ICommand ComandoActualizar
        {
            get;
            private set;
        }

        private void Actualizar()
        {
            this.tareaActualizar.RunWorkerAsync();
        }

        private void TareaActualizarEjecucion(object sender, DoWorkEventArgs e)
        {
            this.EstaActualizando = true;
            this.ListaUsuario.Clear();
            var usuariosTask = App.AdministradorSeguridad.UsuarioObtenerTodos();
            var sucursalesTask = App.AdministradorUbicacion.SucursalObtenerTodos();

            Task.WaitAll(usuariosTask, sucursalesTask);

            foreach (var usuario in usuariosTask.Result)
            {
                this.ListaUsuario.Add(usuario);
            }

            App.AdministradorSeguridad.Usuarios = usuariosTask.Result;
            System.Diagnostics.Trace.WriteLine("TareaActualizarEjecucion(Usuarios) " + System.DateTime.Now.ToLongTimeString());

            App.AdministradorUbicacion.Sucursales = sucursalesTask.Result;
            System.Diagnostics.Trace.WriteLine("TareaActualizarEjecucion(Sucursales) " + System.DateTime.Now.ToLongTimeString());
        }

        private void TareaActualizarTerminado(object sender, RunWorkerCompletedEventArgs e)
        {
            this.EstaActualizando = false;
            System.Diagnostics.Trace.WriteLine("TareaActualizarTerminado " + System.DateTime.Now.ToLongTimeString());
        }

        private async void AbrirUsuario()
        {
            var vistaModeloEquipo = new VistaModeloEquipos(this.usuarioSeleccionado.IdUsuario);
            var vistaEquipos = new VistaEquipos
            {
                BindingContext = vistaModeloEquipo
            };

            await Application.Current.MainPage.Navigation.PushAsync(vistaEquipos);
        }
    }
}
