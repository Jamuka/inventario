# [Sistema de inventario](https://inventariositio.azurewebsites.net/)

![Product Gif](https://media.giphy.com/media/cKJwU8wX2i5vjySoTr/giphy.gif)


## Comandos de terminal

1. Instalar NodeJs desde [NodeJs Official Page](https://nodejs.org/en).
2. Abrir terminal
3. Ir a la raiz del proyecto
4. Ejecutar en CMD o terminal: ```npm install -g @angular/cli```
5. Luego: ```npm install```
6. Finalmente ejecutar: ```npm start```
7. Para prueba local visitar [localhost:4200](localhost:4200)