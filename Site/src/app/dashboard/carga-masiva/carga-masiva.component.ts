import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ServicioTablero } from '../dashboard.service';
import { Notification } from '../../shared/Notification';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-carga-masiva',
  templateUrl: './carga-masiva.component.html',
  styleUrls: ['./carga-masiva.component.css']
})
export class CargaMasivaComponent implements OnInit {
  // -- Variables
  private cargando : boolean;
  public archivoASubir: File = null;
  public VACIA = "./assets/img/image_placeholder.jpg";

  // -- Constructor
  constructor(public modalActivo: NgbActiveModal, 
    private servicioTablero:ServicioTablero) { 
  }

  // -- Propiedades
  get Cargando() : boolean{
    return this.cargando;
  }

  // -- Funciones
  ngOnInit(): void {
  }

  public subir(){
    if(this.archivoASubir == null){
      return;
    }

    this.cargando = true;
    this.servicioTablero.subirArchivo(this.archivoASubir)
      .then(respuesta => 
        {
          this.modalActivo.dismiss({  });
          this.cargando = false;
          Notification.Show('bottom', 'right', 'success', "Archivo guardado satisfactoriamente" )
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.error);
          this.cargando = false;
        });
  }

  cambioEscuchador(archivos: FileList) {
    this.archivoASubir = archivos.item(0);
  }
}
