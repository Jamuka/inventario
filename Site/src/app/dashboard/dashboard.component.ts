import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ServicioEquipo } from '../inventario/equipo/equipo.service';
import { ServicioHistoria } from '../inventario/historia/historia.service';
import { IModeloEquipo } from '../modelos/inventario/equipo.model';
import { IModeloHistoria } from '../modelos/inventario/historia.model';
import { IModeloUsuario } from '../modelos/seguridad/usuario.model';
import { IModeloSucursal, ModeloSucursal } from '../modelos/ubicacion/sucursal.model';
import { ServicioUsuario } from '../seguridad/usuario/usuario.service';
import { ServicioSucursal } from '../ubicacion/sucursal/sucursal.service';
import { Notification } from 'src/app/shared/Notification';
import { ConstantesAplicacion } from '../app-constants';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CargaMasivaComponent } from './carga-masiva/carga-masiva.component';
import { Sort } from '@angular/material/sort';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  // -- Variables
  private cargando : boolean;
  usuarios: IModeloUsuario[];
  equipos: IModeloEquipo[];
  equiposSucursal: IModeloEquipo[];
  equiposOrdenadosSucursal: IModeloEquipo[];
  sucursales: IModeloSucursal[];
  historias: IModeloHistoria[];
  mostrarCargar: boolean = false;
  pagina : number;

  // -- Constructor
  constructor(private servicioUsuario: ServicioUsuario,
  private servicioEquipo: ServicioEquipo,
  private servicioSucursal: ServicioSucursal,
  private servicioHistoria: ServicioHistoria,
  private servicioModal: NgbModal) {
  }

  // -- Propiedades
  get Cargando() : boolean{
    return this.cargando;
  }

  get MostrarCargar() : boolean{
    return this.mostrarCargar;
  }

  // -- Funciones
  ngOnInit(): void {
    this.obtenerTodos();

  let usuario:IModeloUsuario = JSON.parse(localStorage.getItem(ConstantesAplicacion.DatosUsuario));
  let roles = usuario.roles.filter(r=> r.nombre == "Administrador");
  if(roles != null && roles.length > 0){
    this.mostrarCargar = true;
  }
  }

  obtenerTodos(){
    this.cargando = true;
    this.servicioHistoria.obtenerTodos()
      .then(registros =>
        {
          this.historias = registros;
          this.pagina = 1;
          this.cargando = false;
          this.obtenerTodosUsuario();
          this.obtenerTodosEquipo();
          this.obtenerTodosSucursal();          
        })
      .catch((error : HttpErrorResponse) =>
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  obtenerTodosUsuario(){
  this.servicioUsuario.obtenerTodos()
    .then(registros =>
      {        
        this.usuarios = registros;
      })
    .catch((error : HttpErrorResponse) =>
      {
        Notification.Show('bottom', 'right', 'danger', error.message);
      })
  }

  obtenerTodosEquipo(){
  this.servicioEquipo.obtenerTodos(false)
    .then(registros =>
      {        
        this.equipos = registros;
        let sucursal:ModeloSucursal = JSON.parse(localStorage.getItem(ConstantesAplicacion.DatosSucursal));
        if(sucursal !=null)
        {
          this.equiposSucursal = this.equipos.filter(e=> e.idSucursal == sucursal.idSucursal);
        }
        else
        {
          this.equiposSucursal = this.equipos;
        }
        this.equiposOrdenadosSucursal = this.equiposSucursal.slice();
      })
    .catch((error : HttpErrorResponse) =>
      {
        Notification.Show('bottom', 'right', 'danger', error.message);
      })
  }

  obtenerTodosSucursal(){
  this.servicioSucursal.obtenerTodos()
    .then(registros =>
      {
        this.sucursales = registros;
      })
    .catch((error : HttpErrorResponse) =>
      {
        Notification.Show('bottom', 'right', 'danger', error.message);
      })
  }

  public obtenerIdentificacionUsuario(idUsuario){
  if(this.usuarios != null){
    let usuario = this.usuarios.filter(u => u.idUsuario == idUsuario);
    if(usuario == null || usuario.length == 0)
      return null;

    return usuario[0].identificacion;
  }

  return null;
  }

  public obtenerNombreEquipo(idEquipo){
  if(this.equipos != null){
    let equipo = this.equipos.filter(e => e.idEquipo == idEquipo);
    if(equipo == null || equipo.length == 0)
      return null;

    return equipo[0].nombre;
  }

  return null;
  }

  public obtenerNombreSucursal(idSucursal){
  if(this.sucursales != null){
    let sucursal = this.sucursales.filter(s => s.idSucursal == idSucursal);
    if(sucursal == null || sucursal.length == 0)
        return null;

        return sucursal[0].nombre;
  }

  return null;
  }

  public clickSubirArchivo(){
    const modal = this.servicioModal.open(CargaMasivaComponent);
    modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
  }

  private cerrandoFormulario(respuesta){
    if(respuesta == Object(respuesta)){
      this.obtenerTodos();
    }
  }

  sortData(sort: Sort) {
    const data = this.equiposSucursal.slice();
    if (!sort.active || sort.direction === '') {
      this.equiposOrdenadosSucursal = data;
      return;
    }

    this.equiposOrdenadosSucursal = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'idEquipo': return compare(a.idEquipo, b.idEquipo, isAsc);
        case 'sucursal': return compare(this.obtenerNombreSucursal(a.idSucursal), this.obtenerNombreSucursal(b.idSucursal), isAsc);
        case 'nombre': return compare(a.nombre, b.nombre, isAsc);
        case 'modelo': return compare(a.modelo, b.modelo, isAsc);
        case 'activoFijo': return compare(a.activoFijo, b.activoFijo, isAsc);
        case 'marca': return compare(a.marca, b.marca, isAsc);
        default: return 0;
      }
    });
  }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}