import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConstantesAplicacion } from '../app-constants';

@Injectable({
  providedIn: 'root'
})

export class ServicioTablero {
  constructor(private clienteHttp:HttpClient) {     
  }

  subirArchivo(archivo:File){
    const formData = new FormData();
    formData.append("file", archivo, archivo.name);
    return this.clienteHttp.post(ConstantesAplicacion.ApiInventarioTablero + 'subirArchivo', formData).toPromise();
  }
}
