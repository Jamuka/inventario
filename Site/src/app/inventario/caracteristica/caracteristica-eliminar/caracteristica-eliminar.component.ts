import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModeloCaracteristica } from 'src/app/modelos/inventario/caracteristica.model';
import { ServicioCaracteristica } from '../caracteristica.service';
import { Notification } from 'src/app/shared/Notification';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-caracteristica-eliminar',
  templateUrl: './caracteristica-eliminar.component.html',
  styleUrls: ['./caracteristica-eliminar.component.css']
})
export class CaracteristicaEliminarComponent implements OnInit {
  // -- Variables
  public modeloCaracteristica : ModeloCaracteristica;

  // -- Constructor
  constructor(public modalActivo: NgbActiveModal,
    private servicioCaracteristica:ServicioCaracteristica) {       
    }

  // -- Funciones
  ngOnInit(): void {
  }

  public eliminar(){
    this.servicioCaracteristica.eliminar(this.modeloCaracteristica.idCaracteristica)
      .then(respuesta => 
        {
          this.modalActivo.dismiss({ caracteristica : this.modeloCaracteristica });
          Notification.Show('bottom', 'right', 'success', "Caracteristica eliminada satisfactoriamente" )
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.error);
        });
  }
}
