import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModeloEquipo } from 'src/app/modelos/inventario/equipo.model';
import html2canvas from 'html2canvas';
import * as jspdf from 'jspdf';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';
import { DatePipe } from '@angular/common';
import { IModeloEmpresa } from 'src/app/modelos/ubicacion/empresa.model';
import { IModeloSucursal } from 'src/app/modelos/ubicacion/sucursal.model';
import { ServicioImagen } from '../imagen.service';

@Component({
  selector: 'app-equipo-etiqueta-formulario',
  templateUrl: './equipo-etiqueta-formulario.component.html',
  styleUrls: ['./equipo-etiqueta-formulario.component.css']
})

export class EquipoEtiquetaFormularioComponent implements OnInit {
  // -- Variables
  public modeloEquipo : ModeloEquipo;
  elementType = NgxQrcodeElementTypes.IMG;
  correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;  
  empresas:IModeloEmpresa[];
  sucursales: IModeloSucursal[];
  empresa:IModeloEmpresa;
  value = '';
  imageToShow: any;
  isImageLoading: boolean;
  
  // -- Constructor
  constructor(public modalActivo: NgbActiveModal,
    private datePipe: DatePipe,
    private servicioImagen:ServicioImagen) { 
  }

  ngOnInit(): void {
    let sucursal = this.sucursales.filter(s => s.idSucursal == this.modeloEquipo.idSucursal)[0];
    this.empresa = this.empresas.filter(e=>e.idEmpresa = sucursal.idEmpresa)[0];
    this.value= this.modeloEquipo.idEquipo + "," + this.modeloEquipo.activoFijo + "," + this.modeloEquipo.serie + "," + this.datePipe.transform(this.modeloEquipo.fechaCompra, "yyyyMMdd");
    this.obtenerImagenDeServidor();
  }

  public ImprimirA4(){
    var data = document.getElementById('contentToConvert');
    html2canvas(data).then(canvas => {  
      let imgWidth = 208;   
      let imgHeight = canvas.height * imgWidth / canvas.width;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf.jsPDF('p', 'mm', 'a4');
      pdf.addImage(contentDataURL, 'PNG', 0, 0, 100,80);
      pdf.save(this.modeloEquipo.serie + '.pdf');
    }); 
    this.modalActivo.close();
  }

  public Imprimir50(){
    var data = document.getElementById('contentToConvert');
    html2canvas(data).then(canvas => {   
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf.jsPDF('p', 'mm', [50,50]); 
      pdf.addImage(contentDataURL, 'PNG', 0, 0, 65,55);
      pdf.save(this.modeloEquipo.serie + '.pdf');
    }); 
    this.modalActivo.close();
  }

  obtenerImagenDeServidor(){
    this.isImageLoading = true;
      this.servicioImagen.obtener(this.empresa.urlLogo).subscribe(data => {
        this.createImageFromBlob(data);
        this.isImageLoading = false;
      }, error => {
        this.isImageLoading = false;
        console.log(error);
      });
  }

  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
       this.imageToShow = reader.result;
    }, false);
 
    if (image) {
       reader.readAsDataURL(image);
    }
   }
}
