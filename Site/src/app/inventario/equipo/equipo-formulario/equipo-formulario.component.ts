import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModeloEquipo } from 'src/app/modelos/inventario/equipo.model';
import { ServicioEquipo } from '../equipo.service';
import { Notification } from '../../../shared/Notification';
import { HttpErrorResponse } from '@angular/common/http';
import { IModeloTipo } from 'src/app/modelos/inventario/tipo.model';
import { IModeloEstado } from 'src/app/modelos/inventario/estado.model';
import { IModeloSucursal } from 'src/app/modelos/ubicacion/sucursal.model';
import { IModeloUsuario } from 'src/app/modelos/seguridad/usuario.model';
import { ModeloCaracteristica } from 'src/app/modelos/inventario/caracteristica.model';

@Component({
  selector: 'app-equipo-formulario',
  templateUrl: './equipo-formulario.component.html',
  styleUrls: ['./equipo-formulario.component.css']
})

export class EquipoFormularioComponent implements OnInit {
  // -- Variables
  public formularioEquipo : FormGroup;
  public modoAgregar : boolean = true;
  public modeloEquipo : ModeloEquipo;
  tipos: IModeloTipo[];
  estados: IModeloEstado[];
  sucursales: IModeloSucursal[];
  usuarios: IModeloUsuario[];
  caracteristicas : ModeloCaracteristica[];

  // -- Constructor
  constructor(private constructorFormulario : FormBuilder,
    public modalActivo: NgbActiveModal,
    private servicioEquipo:ServicioEquipo) {
  }

  // -- Funciones
  ngOnInit(): void {
    this.formularioEquipo = this.constructorFormulario.group({
      idTipo: [Validators.required],
      idEstado: [Validators.required],
      idSucursal: [Validators.required],
      idUsuario: [Validators.required],
      nombre: ['', Validators.required],
      marca: [''],
      modelo: [''],
      serie: [''],
      activoFijo: [''],      
      numero: ['', Validators.required],
      fechaCompra: [''],
    });

    if(!this.modoAgregar){
      this.formularioEquipo.patchValue(this.modeloEquipo);
      this.ActualizaCaracteristicasPorTipo(this.modeloEquipo.idTipo);
    }
  }

  public guardar(){
    if(this.formularioEquipo.invalid){
      return;
    }

    let modeloEquipoAGuardar : ModeloEquipo = this.formularioEquipo.value;
    if(!this.modoAgregar){
      modeloEquipoAGuardar.idEquipo = this.modeloEquipo.idEquipo;
      modeloEquipoAGuardar.activo = this.modeloEquipo.activo;
    }
    else{
      modeloEquipoAGuardar.activo = true
    }

    modeloEquipoAGuardar.caracteristicas = JSON.parse(JSON.stringify(this.caracteristicas)); 

    this.servicioEquipo.guardar(modeloEquipoAGuardar)
      .then(respuesta => 
        {
          this.modalActivo.dismiss({ equipo : this.modeloEquipo });
          Notification.Show('bottom', 'right', 'success', "Equipo guardado satisfactoriamente" )
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.error);
        });
  }

  public EventoCambioTipo(e) {
    this.ActualizaCaracteristicasPorTipo(e.value);
  }

  ActualizaCaracteristicasPorTipo(idTipo){
    this.caracteristicas = null;
    let tipo = this.tipos.filter(t => t.idTipo == idTipo);
    if(tipo != null){
      if(tipo[0].caracteristicas != null){
        this.caracteristicas = JSON.parse(JSON.stringify(tipo[0].caracteristicas)); 
        for(var i = 0;i<this.caracteristicas.length;i++){
          if(this.modeloEquipo.caracteristicas != null){
            let caracteristica = this.modeloEquipo.caracteristicas.filter(c => c.idCaracteristica == this.caracteristicas[i].idCaracteristica);
            if(caracteristica != null){
              this.caracteristicas[i].valor = caracteristica[0].valor;
            }
          }
        }
      }
    }
  }
}
