import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConstantesAplicacion } from 'src/app/app-constants';
import { IModeloHistoria } from 'src/app/modelos/inventario/historia.model';

@Injectable({
  providedIn: 'root'
})

export class ServicioHistoria {
  constructor(private clienteHttp:HttpClient){    
  }

  obtenerTodos(){
    return this.clienteHttp.get<IModeloHistoria[]>(ConstantesAplicacion.ApiInventarioHistoria + "obtenerTodos").toPromise();
  }

  guardar(modelo: IModeloHistoria){
    return this.clienteHttp.post(ConstantesAplicacion.ApiInventarioHistoria + 'guardar', modelo).toPromise();
  }
}
