import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';   
import { NgxPaginationModule } from 'ngx-pagination';

import { InventarioRoutes } from './inventario-routing.module';
import { EquipoFormularioComponent } from './equipo/equipo-formulario/equipo-formulario.component';
import { EquipoListaComponent } from './equipo/equipo-lista/equipo-lista.component';
import { EstadoListaComponent } from './estado/estado-lista/estado-lista.component';
import { EstadoFormularioComponent } from './estado/estado-formulario/estado-formulario.component';
import { TipoFormularioComponent } from './tipo/tipo-formulario/tipo-formulario.component';
import { TipoListaComponent } from './tipo/tipo-lista/tipo-lista.component';
import { HistoriaListaComponent } from './historia/historia-lista/historia-lista.component';
import { HistoriaFormularioComponent } from './historia/historia-formulario/historia-formulario.component';
import { Router, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { EquipoEtiquetaFormularioComponent } from './equipo/equipo-etiqueta-formulario/equipo-etiqueta-formulario.component';
import { CaracteristicaListaComponent } from './caracteristica/caracteristica-lista/caracteristica-lista.component';
import { CaracteristicaFormularioComponent } from './caracteristica/caracteristica-formulario/caracteristica-formulario.component';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { TipoCaracteristicaFormularioComponent } from './tipo/tipo-caracteristica-formulario/tipo-caracteristica-formulario.component';
import { TreeviewModule } from 'ngx-treeview';
import { EquipoHistoriaListaComponent } from './equipo/equipo-historia-lista/equipo-historia-lista.component';
import { CaracteristicaEliminarComponent } from './caracteristica/caracteristica-eliminar/caracteristica-eliminar.component';

@NgModule({
  declarations: [EquipoFormularioComponent, EquipoListaComponent, EstadoListaComponent, EstadoFormularioComponent, TipoFormularioComponent, TipoListaComponent, HistoriaListaComponent, HistoriaFormularioComponent, EquipoEtiquetaFormularioComponent, CaracteristicaListaComponent, CaracteristicaFormularioComponent, TipoCaracteristicaFormularioComponent, EquipoHistoriaListaComponent, CaracteristicaEliminarComponent],
  imports: [RouterModule.forChild(InventarioRoutes), TreeviewModule.forRoot(), CommonModule, NgbModule, FormsModule, NgxQRCodeModule, ReactiveFormsModule, MaterialModule, NgxPaginationModule ]
})
export class InventarioModule { }
