import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TreeviewConfig, TreeviewItem } from 'ngx-treeview';
import { ModeloTipo } from 'src/app/modelos/inventario/tipo.model';
import { ServicioTipo } from '../tipo.service';
import { Notification } from '../../../shared/Notification';

@Component({
  selector: 'app-tipo-caracteristica-formulario',
  templateUrl: './tipo-caracteristica-formulario.component.html',
  styleUrls: ['./tipo-caracteristica-formulario.component.css']
})

export class TipoCaracteristicaFormularioComponent implements OnInit {
  // -- Variables
  private cargando : boolean;
  public modeloTipo : ModeloTipo;
  public elementos: TreeviewItem[];
  configuracion = TreeviewConfig.create({
    hasAllCheckBox: false,
    hasFilter: false,
    hasCollapseExpand: false,
    decoupleChildFromParent: false,
    maxHeight: 300
  });

  // -- Constructor
  constructor(public modalActivo: NgbActiveModal,
    private servicioTipo:ServicioTipo) {
  }

  // -- Propiedades
  get Cargando() : boolean{
    return this.cargando;
  }

  // -- Funciones
  ngOnInit(): void {
    this.obtenerTodos();
  }

  obtenerTodos(){
    this.cargando = true;
    this.servicioTipo.caracteristicaObtenerTodos(this.modeloTipo.idTipo)
      .then(registros => 
        {
          this.prepararArbol(registros)
          this.cargando = false;
        })
      .catch((error :HttpErrorResponse) =>
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  public guardar(){
    for (let i = 0; i < this.modeloTipo.caracteristicas.length; i++) {
      let caracteristica = this.modeloTipo.caracteristicas[i];
      let elementoEnArbol = this.buscarItemArbol(this.elementos, caracteristica.idCaracteristica);

      if (elementoEnArbol != null) {
        caracteristica.asignado = elementoEnArbol.checked;
      }
    }

    this.servicioTipo.caracteristicaGuardar(this.modeloTipo)
    .then(respuesta => 
      {
        this.modalActivo.dismiss({ tipo : this.modeloTipo });
        Notification.Show('bottom', 'right', 'success', "Tipo guardado satisfactoriamente" )
      })
    .catch((error : HttpErrorResponse) => 
      {
        Notification.Show('bottom', 'right', 'danger', error.error);
      });
  }

  private buscarItemArbol(elementosArbol :TreeviewItem[], idCaracteristica:number): TreeviewItem {
    let encontrado = elementosArbol.find(i => i.value == idCaracteristica);
    if (encontrado != null) {
        return encontrado;
    }
    
    return null;
  }

  private prepararArbol(respuesta:ModeloTipo) {
    this.modeloTipo = respuesta;
    this.elementos = new Array<TreeviewItem>();

    for (let i = 0; i < this.modeloTipo.caracteristicas.length; i++) {
      let caracteristica = this.modeloTipo.caracteristicas[i];

      let newParentItem = new TreeviewItem({ text: caracteristica.nombre, value: caracteristica.idCaracteristica, children: null, checked: caracteristica.asignado });
      this.elementos.push(newParentItem);
    }                   
  }
}
