import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConstantesAplicacion } from 'src/app/app-constants';
import { IModeloTipo, ModeloTipo } from 'src/app/modelos/inventario/tipo.model';

@Injectable({
  providedIn: 'root'
})

export class ServicioTipo {
  constructor(private clienteHttp:HttpClient) { 
  }

  obtenerTodos(){
    return this.clienteHttp.get<IModeloTipo[]>(ConstantesAplicacion.ApiInventarioTipo + 'obtenerTodos' ).toPromise();
  }

  guardar(modelo: IModeloTipo){
    return this.clienteHttp.post(ConstantesAplicacion.ApiInventarioTipo + 'guardar', modelo).toPromise();
  }

  caracteristicaObtenerTodos(idTipo: number){
    let params = new HttpParams().set('idTipo', idTipo.toString());
    return this.clienteHttp.get<IModeloTipo>(ConstantesAplicacion.ApiInventarioTipo + 'caracteristicaobtenertodos', { params : params }).toPromise();
  }

  caracteristicaGuardar(modelo: ModeloTipo){
    return this.clienteHttp.post(ConstantesAplicacion.ApiInventarioTipo + 'caracteristicaGuardar', modelo).toPromise();
  }
}