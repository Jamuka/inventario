export interface IModeloBase{
    fechaInsercion: Date;
}

export class ModeloBase implements IModeloBase {
    fechaInsercion: Date;

    constructor(data: IModeloBase) {
        this.fechaInsercion = data.fechaInsercion;
    }
}
