import { IModeloBase, ModeloBase } from "./base.model";

export interface IModeloCatalogo extends IModeloBase {
    nombre: string;
    activo: boolean;
    fechaActualizacion: Date;
}

export class ModeloCatalogo extends ModeloBase implements IModeloCatalogo{
    nombre: string;
    activo: boolean;
    fechaActualizacion: Date;

    constructor(data: IModeloCatalogo) {
        super(data);
        this.nombre = data.nombre;
        this.activo = data.activo;
        this.fechaActualizacion = data.fechaActualizacion;
    }
}
