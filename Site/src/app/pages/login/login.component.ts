import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ConstantesAplicacion } from 'src/app/app-constants';
import { ModeloEntrarSolicitud } from 'src/app/modelos/seguridad/entrar-solicitud.model';
import { IModeloUsuario } from 'src/app/modelos/seguridad/usuario.model';
import { ModeloEmpresa } from 'src/app/modelos/ubicacion/empresa.model';
import { IModeloSucursal } from 'src/app/modelos/ubicacion/sucursal.model';
import { ServicioUsuario } from 'src/app/seguridad/usuario/usuario.service';
import { Notification } from 'src/app/shared/Notification';
import { ServicioSucursal } from 'src/app/ubicacion/sucursal/sucursal.service';

declare var $: any;

@Component({
    selector: 'app-login-cmp',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit, OnDestroy {
    // -- Variables
    test: Date = new Date();
    private toggleButton: any;
    private sidebarVisible: boolean;
    private nativeElement: Node;
    public formularioEntrar:FormGroup;
    private direccionIP:string;
    sucursales: IModeloSucursal[];
    inventariadores: IModeloUsuario[];
    mostrarSucursales:boolean =false;

    // -- Constructor
    constructor(private constructorFormulario: FormBuilder,
        private servicioUsuario: ServicioUsuario,
        private router:Router,
        private element: ElementRef,
        private servicioSucursal: ServicioSucursal) {
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }

    // -- Metodos
    ngOnInit() {
        var navbar : HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');
        body.classList.add('off-canvas-sidebar');
        const card = document.getElementById('cardLogin');
        setTimeout(function() {
            card.classList.remove('card-hidden');
        }, 700);

        this.formularioEntrar = this.constructorFormulario.group({
            identificacion : ['', Validators.required],
            contrasena : ['', Validators.required],
            idSucursal: []
        });

        this.obtenerTodosSucursal();
        this.obtenerTodosUsuario();
        this.obtenerDireccionIP();
    }    

    sidebarToggle() {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        var sidebar = document.getElementsByClassName('navbar-collapse')[0];
        if (this.sidebarVisible == false) {
            setTimeout(function() {
                toggleButton.classList.add('toggled');
            }, 500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        } else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    }

    ngOnDestroy(){
      const body = document.getElementsByTagName('body')[0];
      body.classList.remove('login-page');
      body.classList.remove('off-canvas-sidebar');
    }

    obtenerTodosSucursal(){
        this.servicioSucursal.obtenerTodos()
          .then(registros =>
            {
              this.sucursales = registros;
            })
          .catch((error : HttpErrorResponse) =>
            {
              Notification.Show('bottom', 'right', 'danger', error.message);
            })
    }

    obtenerTodosUsuario(){
        this.servicioUsuario.obtenerTodosPorRol('Inventariador')
        .then(registros =>
        {
            this.inventariadores = registros;
        })
        .catch((error : HttpErrorResponse) =>
        {
            Notification.Show('bottom', 'right', 'danger', error.message);
        })
    }

    obtenerDireccionIP(){
        this.servicioUsuario.obtenerDireccionIP()
          .then(data => { this.direccionIP = data.ip; })
          .catch((err: HttpErrorResponse) => {  });
    }

    public entrar(){
        if(this.formularioEntrar.invalid){
          return;
        }
  
        let modeloEntrarSolicitudAGuardar: ModeloEntrarSolicitud = this.formularioEntrar.value;
        modeloEntrarSolicitudAGuardar.direccionIP = this.direccionIP;
        this.servicioUsuario.entrar(modeloEntrarSolicitudAGuardar)
            .then(data => 
              {                   
                  localStorage.setItem(ConstantesAplicacion.Llave, data.llave); 
                  localStorage.setItem(ConstantesAplicacion.DatosUsuario, JSON.stringify(data.usuario)); 
                  if(data.empresa != null){
                    localStorage.setItem(ConstantesAplicacion.DatosEmpresa, JSON.stringify(data.empresa)); 
                    localStorage.setItem(ConstantesAplicacion.DatosSucursal, JSON.stringify(data.sucursal)); 
                  }
  
                  if(localStorage.getItem(ConstantesAplicacion.Redireccion)!= null){
                      this.router.navigateByUrl('/' + localStorage.getItem(ConstantesAplicacion.Redireccion), { skipLocationChange:true });
                      localStorage.removeItem(ConstantesAplicacion.Redireccion);
                  }
                  else{
                      this.router.navigateByUrl('/dashboard', { skipLocationChange:true });
                  }
                  
              })
            .catch((err: HttpErrorResponse) => { console.log(err); Notification.Show('bottom', 'right', 'danger', err.message ); }
          );
    }

    onCambioUsuarioEvento(event:any){
        if(this.inventariadores != null){
            let usuario = this.inventariadores.filter(u => u.identificacion == event.target.value);   
            if(usuario != null && usuario.length >0){
                const sucursal = this.sucursales[0];
                this.formularioEntrar.get('idSucursal').setValue(sucursal.idSucursal);
                this.mostrarSucursales = true;
            }
            else{
                this.formularioEntrar.get('idSucursal').setValue(null);
                this.mostrarSucursales = false;
            }

            this.formularioEntrar.updateValueAndValidity();
        }
    }
}
