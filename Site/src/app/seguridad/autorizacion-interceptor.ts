import { HttpInterceptor } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import {tap } from 'rxjs/operators';
import { ConstantesAplicacion } from "../app-constants";

@Injectable({
    providedIn: 'root'
})

export class AutorizacionInterceptor implements HttpInterceptor {
    constructor(private router: Router){
    }
    intercept(req: import("@angular/common/http").HttpRequest<any>, 
            next: import("@angular/common/http").HttpHandler): import("rxjs").Observable<import("@angular/common/http").HttpEvent<any>> {
        if(localStorage.getItem(ConstantesAplicacion.Llave) != null){
            const cloneReq = req.clone({
                headers: req.headers.set('Authorization', ConstantesAplicacion.Bearer + ' ' + localStorage.getItem(ConstantesAplicacion.Llave))
            });
            return next.handle(cloneReq).pipe(
                tap(
                    succ => {},
                    err => {
                        if(err.status == 401){
                            localStorage.removeItem(ConstantesAplicacion.Llave);
                            this.router.navigateByUrl('/pages/login', { skipLocationChange:true });
                        }
                    }
                )
            );
        }

        return next.handle(req.clone());
    }
}
