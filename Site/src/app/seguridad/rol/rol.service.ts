import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConstantesAplicacion } from 'src/app/app-constants';
import { IModeloRol, ModeloRol } from 'src/app/modelos/seguridad/rol.model';

@Injectable({
  providedIn: 'root'
})

export class ServicioRol {
  constructor(private clienteHttp:HttpClient) { 
  }

  obtenerTodos(){
    return this.clienteHttp.get<IModeloRol[]>(ConstantesAplicacion.ApiSeguridadRol + 'obtenerTodos' ).toPromise();
  }

  guardar(modelo: IModeloRol){
    return this.clienteHttp.post(ConstantesAplicacion.ApiSeguridadRol + 'guardar', modelo).toPromise();
  }

  permisoObtenerTodos(idRol: number){
    let params = new HttpParams().set('idRol', idRol.toString());
    return this.clienteHttp.get<IModeloRol>(ConstantesAplicacion.ApiSeguridadRol + 'permisoobtenertodos', { params : params }).toPromise();
  }

  permisoGuardar(modelo: ModeloRol){
    return this.clienteHttp.post(ConstantesAplicacion.ApiSeguridadRol + 'permisoGuardar', modelo).toPromise();
  }
}