import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AutorizacionGuardia } from './autorizacion-guardia';
import { PermisoListaComponent } from './permiso/permiso-lista/permiso-lista.component';
import { RolListaComponent } from './rol/rol-lista/rol-lista.component';
import { UsuarioListaComponent } from './usuario/usuario-lista/usuario-lista.component';

export const SeguridadRoutes: Routes = [
  {
    path: '',
    children:[
      { path: 'permiso', component: PermisoListaComponent, canActivate: [AutorizacionGuardia] },
      { path: 'rol', component: RolListaComponent, canActivate: [AutorizacionGuardia] },
      { path: 'usuario', component: UsuarioListaComponent, canActivate: [AutorizacionGuardia] }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(SeguridadRoutes)],
  exports: [RouterModule]
})

export class SeguridadRoutingModule { }