import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SeguridadRoutes } from './seguridad-routing.module';
import { PermisoListaComponent } from './permiso/permiso-lista/permiso-lista.component';
import { PermisoFormularioComponent } from './permiso/permiso-formulario/permiso-formulario.component';
import { RolFormularioComponent } from './rol/rol-formulario/rol-formulario.component';
import { RolListaComponent } from './rol/rol-lista/rol-lista.component';
import { UsuarioListaComponent } from './usuario/usuario-lista/usuario-lista.component';
import { UsuarioFormularioComponent } from './usuario/usuario-formulario/usuario-formulario.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { RolPermisoFormularioComponent } from './rol/rol-permiso-formulario/rol-permiso-formulario.component';
import { TreeviewModule } from 'ngx-treeview';
import { UsuarioRolFormularioComponent } from './usuario/usuario-rol-formulario/usuario-rol-formulario.component';
import { UsuarioEliminarComponent } from './usuario/usuario-eliminar/usuario-eliminar.component';



@NgModule({
  declarations: [PermisoListaComponent, PermisoFormularioComponent, RolFormularioComponent, RolListaComponent, UsuarioListaComponent, UsuarioFormularioComponent, RolPermisoFormularioComponent, UsuarioRolFormularioComponent, UsuarioEliminarComponent],
  imports: [ RouterModule.forChild(SeguridadRoutes), TreeviewModule.forRoot(), CommonModule, NgbModule, FormsModule, ReactiveFormsModule, MaterialModule, NgxPaginationModule ]
})
export class SeguridadModule { }
