import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import PerfectScrollbar from 'perfect-scrollbar';
import { environment } from 'src/environments/environment';
import { ConstantesAplicacion } from '../app-constants';
import { IModeloPermiso } from '../modelos/seguridad/permiso.model';
import { IModeloUsuario } from '../modelos/seguridad/usuario.model';
import { IModeloEmpresa } from '../modelos/ubicacion/empresa.model';
import { IModeloSucursal } from '../modelos/ubicacion/sucursal.model';
import { ServicioUsuario } from '../seguridad/usuario/usuario.service';

declare const $: any;

@Component({
    selector: 'app-sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: IModeloPermiso[];
    public usuario: IModeloUsuario;
    public empresa: IModeloEmpresa;
    public sucursal:IModeloSucursal;
    public recursosUrl:string = environment.RECURSOS_URL;
    
    constructor(private router: Router,
            private servicioUsuario: ServicioUsuario){        
    }  

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    ngOnInit() {
        this.usuario = JSON.parse(localStorage.getItem(ConstantesAplicacion.DatosUsuario));
        var iempresa = JSON.parse(localStorage.getItem(ConstantesAplicacion.DatosEmpresa));
        if(iempresa != null){
            this.empresa = iempresa;
            this.sucursal = JSON.parse(localStorage.getItem(ConstantesAplicacion.DatosSucursal));
        }

        this.menuItems = this.usuario.permisos;
    }
    updatePS(): void  {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            let ps = new PerfectScrollbar(elemSidebar, { wheelSpeed: 2, suppressScrollX: true });
        }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }

    salir(){
        this.servicioUsuario.salir()
            .then(res => 
          { 
            localStorage.removeItem(ConstantesAplicacion.Llave);
            localStorage.removeItem(ConstantesAplicacion.DatosUsuario);
            localStorage.removeItem(ConstantesAplicacion.DatosEmpresa);
            localStorage.removeItem(ConstantesAplicacion.DatosSucursal);
            this.router.navigate(['/pages/login'], { skipLocationChange:true });   
          })
        .catch((err: HttpErrorResponse) => { console.log(err);  }
        );
    }
}
