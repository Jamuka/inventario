import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConstantesAplicacion } from 'src/app/app-constants';
import { IModeloEmpresa, ModeloEmpresa } from 'src/app/modelos/ubicacion/empresa.model';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

export class ServicioEmpresa {
  constructor(private clienteHttp:HttpClient) { 
  }

  obtenerTodos(){
    return this.clienteHttp.get<IModeloEmpresa[]>(ConstantesAplicacion.ApiUbicacionEmpresa + "obtenerTodos").toPromise();
  }

  guardar(modelo: IModeloEmpresa){
    return this.clienteHttp.post(ConstantesAplicacion.ApiUbicacionEmpresa + 'guardar', modelo).toPromise();
  }
}