import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IModeloEmpresa } from 'src/app/modelos/ubicacion/empresa.model';
import { ModeloSucursal } from 'src/app/modelos/ubicacion/sucursal.model';
import { Notification } from '../../../shared/Notification';
import { ServicioSucursal } from '../sucursal.service';

@Component({
  selector: 'app-sucursal-formulario',
  templateUrl: './sucursal-formulario.component.html',
  styleUrls: ['./sucursal-formulario.component.css']
})

export class SucursalFormularioComponent implements OnInit {
  // -- Variables
  public formularioSucursal : FormGroup;
  public modoAgregar : boolean = true;
  public modeloSucursal : ModeloSucursal;
  empresas: IModeloEmpresa[];

  // -- Constructor
  constructor(private constructorFormulario : FormBuilder,
    public modalActivo: NgbActiveModal,
    private servicioSucursal:ServicioSucursal) {     
  }

  // -- Funciones
  ngOnInit(): void {
    this.formularioSucursal = this.constructorFormulario.group({
      idEmpresa: [Validators.required],
      nombre: ['', Validators.required]
    });

    if(!this.modoAgregar){
      this.formularioSucursal.patchValue(this.modeloSucursal);
    }
  }

  public guardar(){
    if(this.formularioSucursal.invalid){
      return;
    }

    let modeloSucursalAGuardar : ModeloSucursal = this.formularioSucursal.value;
    if(!this.modoAgregar){
      modeloSucursalAGuardar.idSucursal = this.modeloSucursal.idSucursal;
      modeloSucursalAGuardar.activo = this.modeloSucursal.activo;
    }
    else{
      modeloSucursalAGuardar.activo = true
    }

    this.servicioSucursal.guardar(modeloSucursalAGuardar)
      .then(respuesta => 
        {
          this.modalActivo.dismiss({ sucursal : this.modeloSucursal });
          Notification.Show('bottom', 'right', 'success', "Sucursal guardada satisfactoriamente" )
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.error);
        });
  }
}
