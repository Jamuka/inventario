import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConstantesAplicacion } from 'src/app/app-constants';
import { IModeloSucursal } from 'src/app/modelos/ubicacion/sucursal.model';

@Injectable({
  providedIn: 'root'
})

export class ServicioSucursal {
  constructor(private clienteHttp:HttpClient){    
  }

  obtenerTodos(){
    return this.clienteHttp.get<IModeloSucursal[]>(ConstantesAplicacion.ApiUbicacionSucursal + "obtenerTodos").toPromise();
  }

  guardar(modelo: IModeloSucursal){
    return this.clienteHttp.post(ConstantesAplicacion.ApiUbicacionSucursal + 'guardar', modelo).toPromise();
  }
}