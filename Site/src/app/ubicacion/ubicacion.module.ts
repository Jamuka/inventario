import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';   
import { NgxPaginationModule } from 'ngx-pagination';

import { UbicacionRoutes } from './ubicacion-routing.module';
import { EmpresaListaComponent } from './empresa/empresa-lista/empresa-lista.component';
import { EmpresaFormularioComponent } from './empresa/empresa-formulario/empresa-formulario.component';
import { SucursalListaComponent } from './sucursal/sucursal-lista/sucursal-lista.component';
import { SucursalFormularioComponent } from './sucursal/sucursal-formulario/sucursal-formulario.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';

@NgModule({
  declarations: [EmpresaListaComponent, EmpresaFormularioComponent, SucursalListaComponent, SucursalFormularioComponent],  
  imports: [ RouterModule.forChild(UbicacionRoutes), CommonModule, NgbModule, FormsModule, ReactiveFormsModule, MaterialModule, NgxPaginationModule ]
})

export class UbicacionModule { }
