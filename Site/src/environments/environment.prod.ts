export const environment = {
  production: true,
  BASE_URL: 'https://inventarioservicio.azurewebsites.net/api',
  RECURSOS_URL: 'https://inventarioservicio.azurewebsites.net'
};
